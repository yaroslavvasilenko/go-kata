package main

//  good

func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	var max, indexMax int = -1, -1
	for i, dig := range nums {
		if max < dig {
			max = dig
			indexMax = i
		}

	}
	tree := &TreeNode{
		Val:   max,
		Left:  constructMaximumBinaryTree(nums[:indexMax]),
		Right: constructMaximumBinaryTree(nums[indexMax+1:]),
	}
	return tree
}
