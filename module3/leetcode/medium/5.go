package main

//  good

func pairSum(head *ListNode) int {
	slow := head
	fast := head
	for fast != nil && fast.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}
	var prev *ListNode
	for slow != nil {
		nextNode := slow.Next
		slow.Next = prev
		prev = slow
		slow = nextNode
	}
	sum := 0
	for head != nil && prev != nil {
		if head.Val+prev.Val > sum {
			sum = head.Val + prev.Val
		}
		head = head.Next
		prev = prev.Next
	}
	return sum
}

func pairSum2(head *ListNode) int {

	fast, slow := head, head
	var firstPartList []int
	for ; fast != nil; slow, fast = slow.Next, fast.Next.Next {
		firstPartList = append(firstPartList, slow.Val)
	}
	var sum int
	for i := len(firstPartList) - 1; slow != nil; slow, i = slow.Next, i-1 {
		if slow.Val+firstPartList[i] > sum {
			sum = slow.Val + firstPartList[i]
		}

	}

	return sum
}
