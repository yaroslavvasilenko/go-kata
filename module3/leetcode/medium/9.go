package main

import "sort"

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	arr := make([]bool, len(l))
	for i := 0; i < len(l); i++ {
		arr[i] = true
		if r[i]-l[i] > 0 {
			temp := []int{}
			temp = append(temp, nums[l[i]:r[i]+1]...)
			sort.Ints(temp)
			var diff int = temp[1] - temp[0]
			for j := 1; j < len(temp); j++ {
				if temp[j]-temp[j-1] != diff {
					arr[i] = false
					break
				}
			}
		} else {
			arr[i] = false
		}
	}
	return arr
}
