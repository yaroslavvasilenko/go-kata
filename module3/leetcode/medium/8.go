package main

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	arr := make(map[int]bool)
	for _, e := range edges {
		arr[e[1]] = true
	}
	result := make([]int, 0)
	for i := 0; i < n; i++ {
		if !arr[i] {
			result = append(result, i)
		}
	}
	return result
}
