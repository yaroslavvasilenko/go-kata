package main

func groupThePeople(groupSizes []int) [][]int {
	group := make(map[int][]int)
	var groups [][]int
	for id, size := range groupSizes {
		v := group[size]
		if len(v) == size {
			groups = append(groups, v)
			v = []int{}
		}
		v = append(v, id)
		if len(v) == size {
			groups = append(groups, v)
			v = []int{}
		}
		group[size] = v
	}
	return groups
}
