package main

//  good

func goRoot(root *TreeNode, sum int) int {
	if root == nil {
		return sum
	}
	if root.Left == nil && root.Right == nil {
		sum += root.Val
		root.Val = sum
		return sum
	}

	root.Val += goRoot(root.Right, sum)

	return goRoot(root.Left, root.Val)
}

func bstToGst(root *TreeNode) *TreeNode {
	goRoot(root, 0)
	return root
}
