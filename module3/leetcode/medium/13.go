package main

func maxSum(grid [][]int) int {
	m, n := len(grid), len(grid[0])
	ans := 0
	for i := 1; i < m-1; i++ {
		for j := 1; j < n-1; j++ {
			ans = max(ans, grid[i-1][j-1]+
				grid[i-1][j]+
				grid[i-1][j+1]+
				grid[i][j]+
				grid[i+1][j+1]+
				grid[i+1][j]+
				grid[i+1][j-1])
		}
	}
	return ans
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
