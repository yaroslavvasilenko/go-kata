package main

type ListNode struct {
	Val  int
	Next *ListNode
}

//  good

func mergeNodes(head *ListNode) *ListNode {
	for current, next := head, head.Next; next != nil; next = next.Next {
		if next.Val > 0 {
			current.Val += next.Val
		} else {
			if next.Next == nil {
				current.Next = nil
			} else {
				current.Next = next
			}
			current = next
		}
	}

	return head
}
