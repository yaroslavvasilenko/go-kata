package main

func processQueries(queries []int, m int) []int {
	p := make([]int, 0)
	res := make([]int, 0)

	for i := 1; i <= m; i++ {
		p = append(p, i)
	}

	for i := 0; i < len(queries); i++ {
		for ind, v := range p {
			if queries[i] == v {
				res = append(res, ind)

				copy(p[ind+1:], p[ind+1:])
				copy(p[1:ind+1], p[:ind])
				p[0] = v
			}
		}
	}

	return res
}
