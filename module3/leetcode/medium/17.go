package main

func countPoints(points [][]int, queries [][]int) []int {
	res := make([]int, len(queries))
	for i, q := range queries {
		for _, p := range points {
			if (q[0]-p[0])*(q[0]-p[0])+(q[1]-p[1])*(q[1]-p[1]) <= q[2]*q[2] {
				res[i] += 1
			}
		}
	}
	return res
}
