package main

func insert(node *TreeNode, arr *[]*TreeNode) {
	if node == nil {
		return
	}
	insert(node.Left, arr)
	*arr = append(*arr, node)
	insert(node.Right, arr)
}

func balance(nodes []*TreeNode, start, end int) *TreeNode {
	if start > end {
		return nil
	}
	mid := (start + end) / 2
	root := nodes[mid]

	root.Left = balance(nodes, start, mid-1)
	root.Right = balance(nodes, mid+1, end)

	return root
}

func balanceBST(root *TreeNode) *TreeNode {
	var nodes []*TreeNode
	insert(root, &nodes)

	n := len(nodes)
	return balance(nodes, 0, n-1)
}
