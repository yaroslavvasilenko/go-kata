package main

import (
	"fmt"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

var test = &TreeNode{
	Val: 2,
	Left: &TreeNode{
		Val:   5,
		Left:  &TreeNode{Val: 1},
		Right: nil,
	},
}

func main() {
	s := deepestLeavesSum(test)
	fmt.Println(s)

}

//  good

func deepestLeavesSum(root *TreeNode) int {
	mapDeep := make(map[int]int)
	deepSum(root, mapDeep, 0)
	return mapDeep[len(mapDeep)-1]
}

func deepSum(root *TreeNode, m map[int]int, deep int) {
	m[deep] += root.Val

	if root.Left != nil {
		deepSum(root.Left, m, deep+1)
	}
	if root.Right != nil {
		deepSum(root.Right, m, deep+1)
	}
}
