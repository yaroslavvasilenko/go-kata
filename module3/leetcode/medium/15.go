package main

func minPartitions(n string) int {
	maxDigit := '1'

	for _, x := range n {
		if x > maxDigit {
			maxDigit = x

			if maxDigit == '9' {
				break
			}
		}
	}

	return int(maxDigit - '0')
}
