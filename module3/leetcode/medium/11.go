package main

func removeLeafNodes(root *TreeNode, target int) *TreeNode {
	if root == nil {
		return nil
	}
	//post order traversal
	left := removeLeafNodes(root.Left, target)
	right := removeLeafNodes(root.Right, target)

	root.Left = left
	root.Right = right

	if root.Left == nil && root.Right == nil && root.Val == target {
		return nil
	}
	return root
}
