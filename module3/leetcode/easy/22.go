package easy

func smallerNumbersThanCurrent(nums []int) []int {
	res := make([]int, len(nums))
	for ind, digit := range nums {
		for _, digit2 := range nums {
			if digit > digit2 {
				res[ind] += 1
			}
		}
	}
	return res
}
