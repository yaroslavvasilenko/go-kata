package easy

func decode(encoded []int, first int) []int {
	arr := make([]int, 0, len(encoded)+1)
	arr = append(arr, first)
	for ind := range encoded {
		arr = append(arr, arr[ind]^encoded[ind])
	}
	return arr
}
