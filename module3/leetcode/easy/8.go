package easy

func findKthPositive(arr []int, k int) int {
	var res, indArr, countMiss int
	for countMiss != k {
		res++
		if arr[indArr] != res {
			countMiss++
		} else {
			if indArr < len(arr)-1 {
				indArr++
			}
		}
	}
	return res
}
