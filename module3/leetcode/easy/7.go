package easy

import "strings"

func defangIPaddr(address string) string {
	result := strings.Builder{}
	for _, char := range address {
		charByte := byte(char)
		if charByte == '.' {
			result.Write([]byte{'[', '.', ']'})
		} else {
			result.WriteByte(charByte)
		}
	}
	return result.String()
}

func defangIPaddr1(address string) string {
	return strings.ReplaceAll(address, ".", "[.]")

}
