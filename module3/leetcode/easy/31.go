package easy

import "sort"

func sortPeople(names []string, heights []int) []string {
	mapPeople := make(map[int]string, len(names))

	for i := range names {
		mapPeople[heights[i]] = names[i]
	}

	sort.Ints(heights)

	res := make([]string, 0, len(names))
	for i := len(heights) - 1; i >= 0; i-- {
		res = append(res, mapPeople[heights[i]])
	}

	return res
}
