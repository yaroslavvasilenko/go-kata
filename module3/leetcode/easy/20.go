package easy

func kidsWithCandies(candies []int, extraCandies int) []bool {
	var max int
	for ind, candi := range candies {
		if max < candi {
			max = candi
		}
		candies[ind] = candi + extraCandies
	}

	candiBool := make([]bool, len(candies))

	for ind, candi := range candies {
		if max <= candi {
			candiBool[ind] = true
		}
	}
	return candiBool
}
