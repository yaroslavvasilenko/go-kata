package easy

func createTargetArray(nums []int, index []int) []int {
	res := make([]int, 0, len(index))
	for i := range nums {
		res = append(res[:index[i]+1], res[index[i]:]...)
		res[index[i]] = nums[i]
	}
	return res
}
