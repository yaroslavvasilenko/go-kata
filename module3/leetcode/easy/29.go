package easy

func xorOperation(n int, start int) int {
	var nums int
	for i := 0; i < n; i++ {
		nums = nums ^ (start + 2*i)
	}
	return nums
}
