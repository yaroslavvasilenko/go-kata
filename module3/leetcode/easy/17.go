package easy

import "strings"

func mostWordsFound(sentences []string) int {
	var result int
	for _, sentence := range sentences {
		count := strings.Count(sentence, " ")
		if result < count {
			result = count
		}
	}
	return result + 1
}
