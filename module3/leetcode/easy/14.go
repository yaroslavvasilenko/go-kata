package easy

func maximumWealth(accounts [][]int) int {
	var res int
	for _, account := range accounts {
		tempSum := 0
		for _, value := range account {
			tempSum += value
		}
		if res < tempSum {
			res = tempSum
		}
	}
	return res
}
