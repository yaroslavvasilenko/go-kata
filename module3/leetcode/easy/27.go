package easy

const (
	R = 'R'
)

func balancedStringSplit(s string) int {
	var i, res int
	for _, char := range s {
		if char == R {
			i++
		} else {
			i--
		}
		if i == 0 {
			res++
		}
	}
	return res
}
