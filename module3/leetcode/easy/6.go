package easy

import "strings"

var morseMap = map[byte]string{
	'a': ".-",
	'b': "-...",
	'c': "-.-.",
	'd': "-..",
	'e': ".",
	'f': "..-.",
	'g': "--.",
	'h': "....",
	'i': "..",
	'j': ".---",
	'k': "-.-",
	'l': ".-..",
	'm': "--",
	'n': "-.",
	'o': "---",
	'p': ".--.",
	'q': "--.-",
	'r': ".-.",
	's': "...",
	't': "-",
	'u': "..-",
	'v': "...-",
	'w': ".--",
	'x': "-..-",
	'y': "-.--",
	'z': "--..",
}

func uniqueMorseRepresentations(words []string) int {
	morseSet := make(map[string]struct{})
	for _, word := range words {
		strB := strings.Builder{}
		for _, char := range word {
			strB.WriteString(morseMap[byte(char)])
		}
		morseSet[strB.String()] = struct{}{}
	}

	return len(morseSet)
}
