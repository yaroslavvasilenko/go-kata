package easy

func differenceOfSum(nums []int) int {
	var sum, digSum int
	for _, value := range nums {
		sum += value
		for value != 0 {
			digSum += value % 10
			value = value / 10
		}
	}
	return sum - digSum
}
