package easy

func decompressRLElist(nums []int) []int {
	res := make([]int, 0, len(nums))
	for i := 0; i < len(nums); i += 2 {
		for freq := nums[i]; freq != 0; freq-- {
			res = append(res, nums[i+1])
		}
	}
	return res
}
