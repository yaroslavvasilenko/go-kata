package easy

//  можно решить через массив с размерностью 3

func tribonacci(n int) int {
	tri := [3]int{0, 1, 1}
	for i := 0; i < n; i++ {
		tri[i%3] = tri[0] + tri[1] + tri[2]
	}

	return tri[n%3]
}

func tribonacci1(n int) int {
	tri := []int{0, 1, 1}
	for i := 0; i < n; i++ {

		tri = append(tri, tri[i]+tri[i+1]+tri[i+2])
	}

	return tri[n]
}
