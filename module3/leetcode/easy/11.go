package easy

func runningSum(nums []int) []int {
	sum := 0
	for ind, val := range nums {
		sum += val
		nums[ind] = sum
	}
	return nums
}
