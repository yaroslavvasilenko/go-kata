package easy

import "strings"

func finalValueAfterOperations(operations []string) int {
	var x int
	for _, line := range operations {
		if strings.Contains(line, "+") {
			x++
		} else {
			x--
		}
	}
	return x
}
