package easy

func subtractProductAndSum(n int) int {
	var sum, multi int = 0, 1
	var arrDig []int
	for n > 0 {
		arrDig = append(arrDig, n%10)
		n = n / 10
	}

	for _, digit := range arrDig {
		sum += digit
		multi *= digit
	}
	return multi - sum
}
