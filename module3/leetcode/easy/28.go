package easy

func countDigits(num int) int {
	var arr []int
	tempNum, res := num, 0
	for tempNum != 0 {
		arr = append(arr, tempNum%10)
		tempNum /= 10
	}

	for _, digit := range arr {
		if num%digit == 0 {
			res++
		}
	}
	return res
}
