package main

import (
	"fmt"
	"time"
)

type Post struct {
	body        string
	publishDate int64 // Unix timestamp
	next        *Post
}

type Feed struct {
	length int // we'll use it later
	start  *Post
	end    *Post
}

// Реализуйте метод Append для добавления нового поста в конец потока
func (f *Feed) Append(newPost *Post) {
	f.length++
	// ваш код здесь
	if f.start == nil {
		f.start = newPost
		f.end = newPost
		return
	}
	f.end.next = newPost
	f.end = f.end.next

}

// Реализуйте метод Remove для удаления поста по дате публикации из потока
func (f *Feed) Remove(publishDate int64) {
	f.length--
	// ваш код здесь
	if f.start.publishDate == publishDate {
		f.start = f.start.next
		return
	}

	tempStart := f.start

	for {
		if tempStart.next.publishDate == publishDate {
			tempStart.next = tempStart.next.next
			return
		}
		tempStart = tempStart.next
	}

}

// Реализуйте метод Insert для вставки нового поста в поток в соответствии с его датой публикации
func (f *Feed) Insert(newPost *Post) {
	f.length++
	// ваш код здесь
	if f.end.publishDate < newPost.publishDate {
		f.end.next = newPost
		f.end = newPost
	}

	if f.start.publishDate > newPost.publishDate {
		newPost.next = f.start
		f.start = newPost
	}

	tempStart := f.start.next
	for {
		if tempStart.publishDate < newPost.publishDate {
			tempStart.next, newPost.next = newPost, tempStart.next
			return
		}
		tempStart = tempStart.next
	}
}

// Реализуйте метод Inspect для вывода информации о потоке и его постах
func (f *Feed) Inspect() {
	// ваш код здесь
	fmt.Printf("Feed Length: %v\n", f.length)
	tempStart := f.start
	for i := 0; i < f.length; i++ {
		fmt.Printf("Item: %v - %+v\\\n", i, tempStart)
		tempStart = tempStart.next
	}

}

func main() {
	rightNow := time.Now().Unix()
	f := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 20,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 30,
	}
	f.Append(p1)
	f.Append(p2)
	f.Append(p3)
	f.Append(p4)

	f.Inspect()

	newPost := &Post{
		body:        "This is a new post",
		publishDate: rightNow + 15,
	}
	f.Insert(newPost)
	f.Inspect()
}
