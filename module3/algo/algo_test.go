package algo

import (
	"math/rand"
	"reflect"
	"sort"
	"testing"
	"time"
)

func TestBubbleSort(t *testing.T) {
	type args struct {
		data []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "sort reversed slice",
			args: args{
				data: []int{55, 34, 21, 13, 8, 5, 3, 2, 1, 1},
			},
			want: []int{1, 1, 2, 3, 5, 8, 13, 21, 34, 55},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BubbleSort(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BubbleSort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkBubbleSort(b *testing.B) {
	data := []int{55, 34, 21, 13, 8, 5, 3, 2, 1, 1}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(data)
	}
}

func TestQuickSort(t *testing.T) {
	data := randomData(100, 1000)
	sorted := data()
	sort.Ints(sorted)

	type args struct {
		data []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		// TODO: Add test cases.
		{name: "sort reverse slice",
			args: args{
				data: []int{55, 34, 21, 13, 8, 5, 3, 2, 1, 1}},
			want: []int{1, 1, 2, 3, 5, 8, 13, 21, 34, 55},
		},
		{name: "random 1000 numbers",
			args: args{
				data: data(),
			},
			want: sorted,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := QuickSort(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("QuickSort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkBubbleSort2(b *testing.B) {
	dataSet := generateData(b.N, 1000, 5000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(dataSet[i])
	}
}

func BenchmarkQuickSort(b *testing.B) {
	dataSet := generateData(b.N, 1000, 5000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		QuickSort(dataSet[i])
	}
}

func Test_mergeSort(t *testing.T) {
	data := randomData(100, 1000)
	sorted := data()
	sort.Ints(sorted)
	type args struct {
		data []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		// TODO: Add test cases.
		{name: "sort reverse slice",
			args: args{
				data: []int{55, 34, 21, 13, 8, 5, 3, 2, 1, 1}},
			want: []int{1, 1, 2, 3, 5, 8, 13, 21, 34, 55},
		},
		{name: "random 1000 numbers",
			args: args{
				data: data(),
			},
			want: sorted,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MergeSort(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MergeSort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkMergeSort(b *testing.B) {
	dataSet := generateData(b.N, 1000, 5000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MergeSort(dataSet[i])
	}
}

func randomData(n, max int) func() []int {
	var data []int

	return func() []int {
		if data != nil {
			return data
		}
		rand.Seed(time.Now().UnixNano())
		for i := 0; i < n; i++ {
			data = append(data, rand.Intn(max))
		}
		return data
	}
}

func generateData(n, count, maxValue int) [][]int {
	var data [][]int
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		var cycleData []int
		for j := 0; j < count; j++ {
			cycleData = append(cycleData, rand.Intn(maxValue))
		}
		data = append(data, cycleData)
	}
	return data
}
