package app

import (
	"fmt"
	"log"

	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/cli/model"
)

type TodoServiceInterface interface {
	ListTodos() ([]model.Task, error)
	CreateTodo(title string) (*model.Task, error)
	CompleteTodo(id string) error
	RemoveTodo(id string) error
	UpdateTodo(id, title string) (*model.Task, error)
}

const menu = `
	"Введите команду и нажмите Enter"
	"show - Показать список задач"
	"add - Добавить задачу"
	"remove - Удалить задачу"
	"change Редактировать задачу"
	"complete Завершить задачу"
	"exit Выход"
`

type app struct {
	service TodoServiceInterface
}

func NewApp(s TodoServiceInterface) *app {
	return &app{
		service: s,
	}
}

func (a *app) Start() error {
	fmt.Print(menu)
	err := a.menuHandler()
	if err != nil {
		log.Println(err)
	}
	return nil
}

func (a *app) menuHandler() error {
	for {
		var stopFlag bool
		text, err := readConsole()
		if err != nil {
			return err
		}
		switch text {
		case "show":
			a.showTasks()
		case "add":
			a.addTask()
		case "remove":
			a.deleteTask()
		case "change":
			a.changeTask()
		case "complete":
			a.taskComplete()
		case "exit":
			stopFlag = true
		default:
			fmt.Println("не знаю такой команды")
			fmt.Println("")
			fmt.Print(menu)
		}

		if stopFlag {
			break
		}

	}

	return nil
}
