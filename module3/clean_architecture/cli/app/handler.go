package app

import (
	"fmt"
	"log"
)

func (a *app) showTasks() {
	list, err := a.service.ListTodos()
	if err != nil {
		log.Println(err)
		return
	}

	prettyPrint(list)
}

func (a *app) addTask() {

	fmt.Println("Введите название задачи")

	text, err := readConsole()
	if err != nil {
		log.Println(err)
	}
	task, err := a.service.CreateTodo(text)
	if err != nil {
		log.Println(err)
		return
	}

	prettyPrint(task)
}

func (a *app) deleteTask() {
	log.Println("введите id задачи")
	text, err := readConsole()
	if err != nil {
		log.Println(err)
		return
	}

	err = a.service.RemoveTodo(text)
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println("Удалена задача " + text)
}

func (a *app) changeTask() {
	fmt.Println("введите id задачи")
	textID, err := readConsole()
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println("введите новый заголовок")
	textTitle, err := readConsole()
	if err != nil {
		log.Println(err)
		return
	}
	task, err := a.service.UpdateTodo(textID, textTitle)
	if err != nil {
		log.Println(err)
		return
	}
	prettyPrint(task)
}

func (a *app) taskComplete() {
	fmt.Println("введите id задачи")
	text, err := readConsole()
	if err != nil {
		log.Println(err)
		return
	}
	err = a.service.CompleteTodo(text)
	if err != nil {
		log.Println(err)
	}
	fmt.Println("готово")
}

func (a *app) Hello() {
	log.Println("Hello")
}
