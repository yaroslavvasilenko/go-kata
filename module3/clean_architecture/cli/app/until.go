package app

import (
	"encoding/json"
	"fmt"
	"log"
)

func prettyPrint(data interface{}) {
	var p []byte
	p, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Printf("%s \n", p)
}

func readConsole() (string, error) {
	var text string
	_, err := fmt.Scanln(&text)
	if err != nil {
		return "", err
	}
	return text, nil
}
