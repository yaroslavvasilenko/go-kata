package until

import "os"

func ExistFile(filePath string) error {
	if _, err := os.Stat(filePath); err != nil {
		if os.IsNotExist(err) {
			// file does not exist
			f, err := os.Create(filePath)
			if err != nil {
				return err
			}
			_, err = f.Write([]byte("[]"))
			if err != nil {
				return err
			}
			err = f.Close()
			if err != nil {
				return err
			}

		} else {
			// other error
			return err
		}
	}
	return nil
}
