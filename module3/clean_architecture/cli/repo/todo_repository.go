package repo

import (
	"encoding/json"
	"errors"
	"io"
	"os"

	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/cli/model"
)

var NotFoundTask = errors.New("not found task")

// TaskRepository is a repository interface for tasks

//  Обязательно этот интерфейс должен полностью соотвестыввать TodoServiceInterface
//  допустим UpdateTask как мне убрать? что бы интерфейс соотвествовал?
//  ощушение что я 2 аза одно и тоже написал пока что

// FileTaskRepository is a file-based implementation of TaskRepository
type FileTaskRepository struct {
	FilePath string
}

func NewFileTaskRepository(f string) *FileTaskRepository {
	return &FileTaskRepository{f}
}

// GetTasks returns all tasks from the repository
func (repo *FileTaskRepository) GetTasks() ([]model.Task, error) {
	var tasks []model.Task

	file, err := os.Open(repo.FilePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(content, &tasks); err != nil {
		return nil, err
	}

	return tasks, nil
}

// GetTask returns a single task by its ID
func (repo *FileTaskRepository) GetTask(id int) (*model.Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return nil, err
	}

	var task *model.Task
	var found bool
	for _, t := range tasks {
		if t.ID == id {
			found = true
			task = &t
			break
		}
	}

	if !found {
		return nil, NotFoundTask
	}

	return task, nil
}

// CreateTask adds a new task to the repository
func (repo *FileTaskRepository) CreateTask(task model.Task) (model.Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	task.ID = len(tasks) + 1
	tasks = append(tasks, task)

	if err := repo.saveTasks(tasks); err != nil {
		return task, err
	}

	return task, nil
}

// UpdateTask updates an existing task in the repository
func (repo *FileTaskRepository) UpdateTask(task model.Task) (*model.Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return nil, err
	}

	var found bool
	for i, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			found = true

			err = repo.saveTasks(tasks)
			if err != nil {
				return nil, err
			}
			break
		}
	}

	if !found {
		return nil, NotFoundTask
	}

	return &task, nil
}

func (repo *FileTaskRepository) DeleteTask(id int) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}

	var found bool
	for i, t := range tasks {
		if t.ID == id {
			found = true
			tasks = append(tasks[:i], tasks[i+1:]...)
			break
		}
	}

	if !found {
		return NotFoundTask
	}

	if err := repo.saveTasks(tasks); err != nil {
		return err
	}
	return nil
}

func (repo *FileTaskRepository) saveTasks(task []model.Task) error {
	f, err := os.OpenFile(repo.FilePath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		return err
	}

	taskByte, err := json.Marshal(task)
	if err != nil {
		return err
	}

	_, err = f.Write(taskByte)
	if err != nil {
		return err
	}

	if err := f.Close(); err != nil {
		return err
	}
	return nil
}
