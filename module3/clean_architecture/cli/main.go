package main

import (
	"log"

	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/cli/until"

	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/cli/app"
	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/cli/repo"
	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/cli/service"
)

// Repo layer
func main() {
	filePath := "./cliJSON"
	err := until.ExistFile(filePath)
	if err != nil {
		log.Println(err)
		return
	}

	f := repo.NewFileTaskRepository(filePath)
	qwe := service.NewTodoService(f)
	cliApp := app.NewApp(qwe)
	err = cliApp.Start()
	if err != nil {
		log.Println(err)
	}
}
