package test

import (
	"encoding/json"
	"os"
	"reflect"
	"testing"

	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/cli/repo"

	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/cli/model"
)

var filePath = "./testFile"
var fileTaskRepo = &repo.FileTaskRepository{
	FilePath: filePath,
}

var (
	task1 = model.Task{ID: 1,
		Title:       "one",
		Description: "",
		Status:      0,
	}
	task2 = model.Task{ID: 2,
		Title:       "two",
		Description: "",
		Status:      0,
	}
	task3 = model.Task{ID: 3,
		Title:       "three",
		Description: "",
		Status:      0,
	}
	task4 = model.Task{ID: 4,
		Title:       "four",
		Description: "",
		Status:      0,
	}
)
var taskTest = []model.Task{
	task1,
	task2,
	task3,
	task4,
}

func changeTask(task model.Task, title, desc *string, statusTask *model.StatusTask) *model.Task {
	if title != nil {
		task.Title = *title
	}
	if desc != nil {
		task.Description = *desc
	}
	if statusTask != nil {
		task.Status = *statusTask
	}
	return &task
}

func pointStatus(st model.StatusTask) *model.StatusTask {
	return &st
}

func createTestFileJson(tasks []model.Task) (*os.File, error) {

	f, err := os.Create(filePath)
	if err != nil {
		return nil, err
	}
	taskByte, err := json.Marshal(tasks)
	_, err = f.Write(taskByte)
	if err != nil {
		return nil, err
	}

	return f, err

}

func pointerStr(s string) *string {
	return &s
}

func TestFileTaskRepository_GetTasks(t *testing.T) {
	type fields struct {
		tasks []model.Task
		repo  *repo.FileTaskRepository
	}
	tests := []struct {
		name    string
		fields  fields
		want    []model.Task
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "default",
			fields: fields{tasks: taskTest,
				repo: fileTaskRepo},
			want:    taskTest,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			f, err := createTestFileJson(tt.fields.tasks)
			got, err := tt.fields.repo.GetTasks()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTasks() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTasks() got = %v, want %v", got, tt.want)
			}

			if err := f.Close(); err != nil {
				t.Error(err)
			}

			if err := os.Remove(filePath); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestFileTaskRepository_GetTask(t *testing.T) {
	type fields struct {
		tasks []model.Task
		repo  *repo.FileTaskRepository
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *model.Task
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "default",
			fields: fields{
				tasks: taskTest,
				repo:  fileTaskRepo,
			},
			args:    args{id: 2},
			want:    &taskTest[1],
			wantErr: false,
		},
		{
			name: "first task",
			fields: fields{
				tasks: taskTest,
				repo:  fileTaskRepo,
			},
			args:    args{id: 1},
			want:    &taskTest[0],
			wantErr: false,
		},
		{
			name: "last task",
			fields: fields{
				tasks: taskTest,
				repo:  fileTaskRepo,
			},
			args:    args{id: 4},
			want:    &taskTest[3],
			wantErr: false,
		},
		{
			name: "error not found",
			fields: fields{
				tasks: taskTest,
				repo:  fileTaskRepo,
			},
			args:    args{id: 5},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f, err := createTestFileJson(tt.fields.tasks)

			got, err := tt.fields.repo.GetTask(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTask() got = %v, want %v", got, tt.want)
			}

			if err := f.Close(); err != nil {
				t.Error(err)
			}

			if err := os.Remove(filePath); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestFileTaskRepository_CreateTask(t *testing.T) {
	type fields struct {
		repo *repo.FileTaskRepository
	}
	type args struct {
		tasks []model.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []model.Task
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name:    "regular case",
			fields:  fields{fileTaskRepo},
			args:    args{taskTest},
			want:    taskTest,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f, err := createTestFileJson([]model.Task{})
			if err != nil {
				t.Error(err)
			}
			for _, task := range tt.args.tasks {
				_, err = tt.fields.repo.CreateTask(task)
			}
			for i, task := range tt.args.tasks {

				checkTask, err := tt.fields.repo.GetTask(task.ID)
				if (err != nil) != tt.wantErr {
					t.Errorf("CreateTask() error = %v, wantErr %v", err, tt.wantErr)
				}
				if !reflect.DeepEqual(*checkTask, tt.want[i]) {
					t.Errorf("CreateTask() got = %v, want %v", *checkTask, tt.want[i])
				}
			}

			if err := f.Close(); err != nil {
				t.Error(err)
			}

			if err := os.Remove(filePath); err != nil {
				t.Error(err)
			}

		})
	}
}

func TestFileTaskRepository_UpdateTask(t *testing.T) {
	type fields struct {
		tasks []model.Task
		repo  *repo.FileTaskRepository
	}
	type args struct {
		task       *model.Task
		targetTask int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *model.Task
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "regular case",
			fields: fields{
				tasks: taskTest,
				repo:  fileTaskRepo,
			},
			args: args{
				task:       changeTask(task2, pointerStr("hello"), pointerStr("hello"), pointStatus(1)),
				targetTask: task2.ID,
			},
			want:    changeTask(task2, pointerStr("hello"), pointerStr("hello"), pointStatus(1)),
			wantErr: false,
		},
		{
			name: "error not found task",
			fields: fields{
				tasks: taskTest,
				repo:  fileTaskRepo,
			},
			args: args{
				task:       &task1,
				targetTask: 19,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f, err := createTestFileJson(tt.fields.tasks)
			if err != nil {
				t.Error(err)
			}

			_, err = tt.fields.repo.UpdateTask(*tt.args.task)
			if err != nil {
				t.Error(err)
			}

			_, err = tt.fields.repo.UpdateTask(*tt.args.task)
			got, err := tt.fields.repo.GetTask(tt.args.targetTask)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateTask() got = %v, want %v", got, tt.want)
			}

			if err := f.Close(); err != nil {
				t.Error(err)
			}

			if err := os.Remove(filePath); err != nil {
				t.Error(err)
			}
		})
	}
}

func TestFileTaskRepository_RemoveTask(t *testing.T) {
	type fields struct {
		tasks []model.Task
		repo  *repo.FileTaskRepository
	}
	type args struct {
		task model.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []model.Task
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "regular case",
			fields: fields{
				tasks: taskTest,
				repo:  fileTaskRepo,
			},
			args:    args{task2},
			want:    []model.Task{task1, task3, task4},
			wantErr: false,
		},
		{
			name: "error not found task",
			fields: fields{
				tasks: []model.Task{task1, task3, task4},
				repo:  fileTaskRepo,
			},
			args:    args{task2},
			want:    []model.Task{task1, task3, task4},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f, err := createTestFileJson(tt.fields.tasks)

			err = tt.fields.repo.DeleteTask(tt.args.task.ID)

			got, _ := tt.fields.repo.GetTasks()

			if (err != nil) != tt.wantErr && err != repo.NotFoundTask {
				t.Errorf("RemoveTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RemoveTask() got = %v, want %v", got, tt.want)
			}

			if err := f.Close(); err != nil {
				t.Error(err)
			}

			if err := os.Remove(filePath); err != nil {
				t.Error(err)
			}
		})
	}
}
