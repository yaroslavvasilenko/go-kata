package test

import (
	"os"
	"reflect"
	"strconv"
	"testing"

	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/cli/service"

	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/cli/model"
)

var todoServ = service.TodoService{
	fileTaskRepo,
}

func Test_todoService_CreateTodo(t *testing.T) {
	type fields struct {
		repo service.TodoService
	}
	type args struct {
		tasks []model.Task
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []model.Task
		wantErr bool
	}{
		{
			name:    "regular case",
			fields:  fields{todoServ},
			args:    args{taskTest},
			want:    taskTest,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f, err := createTestFileJson([]model.Task{})
			if err != nil {
				t.Error(err)
			}
			for _, task := range tt.args.tasks {
				_, err = tt.fields.repo.CreateTodo(task.Title)
			}

			if (err != nil) != tt.wantErr {
				t.Errorf("CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
			}

			got, err := tt.fields.repo.Repo.GetTasks()
			if err != nil {
				t.Error(err)
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateTodo() got = %v, want %v", got, tt.want)
			}

			if err := f.Close(); err != nil {
				t.Error(err)
			}

			if err := os.Remove(filePath); err != nil {
				t.Error(err)
			}
		})
	}
}

func Test_todoService_CompleteTodo(t *testing.T) {
	type fields struct {
		repo       service.TodoService
		tasks      []model.Task
		targetTask model.Task
	}
	type args struct {
		todo model.Task
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []model.Task
		wantErr bool
	}{
		{
			name: "regular case",
			fields: fields{
				repo:       todoServ,
				tasks:      taskTest,
				targetTask: task2,
			},
			want: func() []model.Task {
				te := taskTest
				te[1].Status = model.StatusCompleted
				return te
			}(),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f, err := createTestFileJson(tt.fields.tasks)
			if err != nil {
				t.Error(err)
			}
			id := strconv.Itoa(tt.fields.targetTask.ID)
			err = tt.fields.repo.CompleteTodo(id)

			if (err != nil) != tt.wantErr {
				t.Errorf("CompleteTodo() error = %v, wantErr %v", err, tt.wantErr)
			}

			got, err := tt.fields.repo.ListTodos()
			if err != nil {
				t.Error(err)
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CompleteTodo() got = %v, want %v", got, tt.want)
			}

			if err := f.Close(); err != nil {
				t.Error(err)
			}

			if err := os.Remove(filePath); err != nil {
				t.Error(err)
			}
		})
	}
}
