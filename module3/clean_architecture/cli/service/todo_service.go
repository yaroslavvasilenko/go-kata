package service

import (
	"strconv"

	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/cli/model"
)

type TaskRepository interface {
	GetTasks() ([]model.Task, error)
	GetTask(id int) (*model.Task, error)
	CreateTask(task model.Task) (model.Task, error)
	UpdateTask(task model.Task) (*model.Task, error)
	DeleteTask(id int) error
}

type TodoService struct {
	Repo TaskRepository
}

func (s *TodoService) CreateTodo(title string) (*model.Task, error) {
	task := model.Task{
		Title: title,
	}

	t, err := s.Repo.CreateTask(task)
	if err != nil {
		return nil, err
	}
	return &t, nil
}

func (s *TodoService) UpdateTodo(id, title string) (*model.Task, error) {

	idInt, err := strconv.Atoi(id)
	if err != nil {
		return nil, err
	}
	t, err := s.Repo.GetTask(idInt)
	if err != nil {
		return nil, err
	}
	if title != "_" {
		t.Title = title
	}

	return s.Repo.UpdateTask(*t)
}

func NewTodoService(f TaskRepository) *TodoService {
	return &TodoService{Repo: f}
}

func (s *TodoService) ListTodos() ([]model.Task, error) {
	return s.Repo.GetTasks()
}

func (s *TodoService) CompleteTodo(id string) error {
	idInt, err := strconv.Atoi(id)
	if err != nil {
		return err
	}
	t, err := s.Repo.GetTask(idInt)
	if err != nil {
		return err
	}

	t.Status = model.StatusCompleted
	_, err = s.Repo.UpdateTask(*t)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) RemoveTodo(id string) error {
	idInt, err := strconv.Atoi(id)
	if err != nil {
		return err
	}
	err = s.Repo.DeleteTask(idInt)
	return err
}
