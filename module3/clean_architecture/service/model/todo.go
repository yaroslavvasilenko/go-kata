package model

type Task struct {
	ID          int        `json:"id"`
	Title       string     `json:"title"`
	Description string     `json:"description"`
	Status      StatusTask `json:"status"`
}

//  А я тут точно правильно тип использую для ограничения использования и удобства))))?

type StatusTask int

const (
	StatusIncomplete StatusTask = iota
	StatusCompleted
)
