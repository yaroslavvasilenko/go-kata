package service

import (
	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/service/repo"
)

type TodoServiceInterface interface {
	ListTodos() ([]model.Task, error)
	CreateTodo(title string) error
	CompleteTodo(todo model.Task) error
	RemoveTodo(todo model.Task) error
}

type TodoService struct {
	Repository repo.FileTaskRepository
}

func (s *TodoService) ListTodos() ([]model.Task, error) {
	return s.Repository.GetTasks()
}

func (s *TodoService) CreateTodo(title string) error {
	task := model.Task{
		Title: title,
	}

	_, err := s.Repository.CreateTask(task)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) CompleteTodo(todo model.Task) error {
	todo.Status = model.StatusCompleted
	_, err := s.Repository.UpdateTask(todo)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) RemoveTodo(todo model.Task) error {
	err := s.Repository.DeleteTask(todo.ID)
	return err
}
