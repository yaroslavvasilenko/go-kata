package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/yaroslavvasilenko/go-kata/module3/clean_architecture/repo/repository"
)

const filePath = "./module3/clean_architecture/repo/test.json"

func main() {
	err := repository.ExistFile(filePath)
	if err != nil {
		log.Println(err)
		return
	}

	f, err := os.OpenFile(filePath, os.O_APPEND|os.O_RDWR, 0644)
	if err != nil {
		log.Println(err)
		return
	}
	defer func() {
		err := f.Close()
		if err != nil {
			log.Println(err)
		}
	}()

	userRep := repository.NewUserRepository(f)
	tt := repository.User{
		ID:   23,
		Name: "Pop",
	}

	err = userRep.Save(tt)
	if err != nil {
		log.Println(err)
		return
	}

	i, err := userRep.Find(tt.ID)
	if err != nil {
		log.Println(err)
	}
	log.Println(i)
	fmt.Println(userRep.FindAll())
}
