package repository

import (
	"encoding/json"
	"os"
	"reflect"
	"testing"
)

func TestUserRepository_Save(t *testing.T) {
	type args struct {
		record []interface{}
	}
	tests := []struct {
		name    string
		want    []byte
		args    args
		wantErr bool
	}{

		{
			name:    "regular save",
			want:    interfaceToUser(testCase),
			args:    args{record: userToInterface(testCase)},
			wantErr: false,
		},
		{
			name:    "empty save",
			want:    []byte{},
			args:    args{record: []interface{}{}},
			wantErr: true,
		},
		{
			name:    "non User save",
			want:    []byte{'w'},
			args:    args{record: []interface{}{"w"}},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			filePath := "./tempTest"
			f, err := os.Create(filePath)
			if err != nil {
				t.Error(err)
				return
			}

			r := &UserRepository{
				File: f,
			}
			var worldErr error
			for _, us := range tt.args.record {
				err := r.Save(us)
				if err != nil {
					worldErr = err
				}
				if (err != nil) != tt.wantErr {
					t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
				}
			}

			if worldErr == nil {
				fileData, err := os.ReadFile(filePath)
				if err != nil {
					t.Error(err)
				}
				if !reflect.DeepEqual(fileData, tt.want) {
					t.Errorf("Search() = %v, want %v", fileData, tt.want)
				}
			}

			err = f.Close()
			if err != nil {
				t.Error(err)
			}

			err = os.Remove(filePath)
			if err != nil {
				t.Error(err)
			}

		})
	}
}

func TestUserRepository_Find(t *testing.T) {
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  []interface{}
		args    args
		want    User
		wantErr bool
	}{
		{
			name:    "regular find",
			fields:  userToInterface(testCase),
			args:    args{id: testCase[0].ID},
			want:    testCase[0],
			wantErr: false,
		},
		{
			name:    "regular find",
			fields:  userToInterface(testCase),
			args:    args{id: testCase[1].ID},
			want:    testCase[1],
			wantErr: false,
		},
		{
			name:    "error not found",
			fields:  userToInterface(testCase),
			args:    args{id: 123456789},
			want:    testCase[0],
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			filePath := "./tempTest"
			f, err := os.Create(filePath)
			if err != nil {
				t.Error(err)
				return
			}
			r := &UserRepository{
				File: f,
			}
			for _, us := range tt.fields {
				err := r.Save(us)
				if err != nil {
					t.Error(err)
					return
				}
			}
			got, err := r.Find(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err == nil {
				gotUser, _ := got.(User)
				if !reflect.DeepEqual(gotUser, tt.want) {
					t.Errorf("Find() got = %v, want %v", gotUser, tt.want)
				}
			}
			err = f.Close()
			if err != nil {
				t.Error(err)
			}

			err = os.Remove(filePath)
			if err != nil {
				t.Error(err)
			}

		})
	}
}

func interfaceToUser(u []User) (res []byte) {
	for _, us := range u {
		asd, _ := json.Marshal(us)
		res = append(res, asd...)
		res = append(res, '\n')
	}

	return
}

func TestUserRepository_FindAll(t *testing.T) {
	type args struct {
		record []interface{}
	}
	tests := []struct {
		name    string
		want    []User
		args    args
		wantErr bool
	}{
		{
			name:    "regular find all",
			want:    testCase,
			args:    args{record: userToInterface(testCase)},
			wantErr: false,
		},
		{
			name:    "nil file",
			want:    nil,
			args:    args{record: nil},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			filePath := "./tempTest"
			f, err := os.Create(filePath)
			if err != nil {
				t.Error(err)
				return
			}
			r := &UserRepository{
				File: f,
			}
			if tt.args.record != nil {
				for _, us := range tt.args.record {
					err := r.Save(us)
					if err != nil {
						t.Error(err)
						return
					}
				}
			}
			got, err := r.FindAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			var userSlice []User
			if got == nil {
				userSlice = nil
			}
			if err == nil {
				for _, user := range got {
					gotUser, _ := user.(User)
					userSlice = append(userSlice, gotUser)
				}

			}
			if !reflect.DeepEqual(userSlice, tt.want) {
				t.Errorf("Find() got = %v, want %v", userSlice, tt.want)
			}
			err = f.Close()
			if err != nil {
				t.Error(err)
			}

			err = os.Remove(filePath)
			if err != nil {
				t.Error(err)
			}
		})
	}
}

var testCase = []User{
	{
		ID:   382395,
		Name: "Maria",
	},
	{
		ID:   902349,
		Name: "Vlad",
	},
}

func userToInterface(u []User) (res []interface{}) {
	for _, us := range u {
		res = append(res, us)
	}
	return
}
