package repository

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"os"
	"strconv"
	"strings"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File *os.File
}

func NewUserRepository(file *os.File) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record interface{}) error {
	// logic to write a record as a JSON object to a file at r.FilePath
	user, ok := record.(User)
	if !ok {
		return errors.New("not user")
	}
	_, err := r.Find(user.ID)
	if err == nil {

		return errors.New("exist user")
	}

	byteUser, err := json.Marshal(user)
	if err != nil {
		return err
	}

	_, err = r.File.Write(append(byteUser, '\n'))

	return err

}

func (r *UserRepository) Find(id int) (interface{}, error) {
	// logic to read a JSON object from a file at r.FilePath and return the corresponding record
	_, err := r.File.Seek(0, 0)
	if err != nil {
		return nil, err
	}
	var user User

	reader := bufio.NewReader(r.File)
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				return nil, errors.New("not found user")
			}
			return nil, err
		}

		fields := strings.Split(line, ",")

		asd, err := strconv.Atoi(strings.Trim(strings.Split(fields[0], ":")[1], "\""))
		if err != nil {
			return nil, err
		}

		if asd == id {
			dsa := strings.Trim(strings.Split(fields[1], "\":\"")[1], "\"}\n")
			user = User{
				ID:   asd,
				Name: dsa,
			}
			break
		}

	}

	return user, nil
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	// logic to read all JSON objects from a file at r.FilePath and return a slice of records
	_, err := r.File.Seek(0, 0)
	if err != nil {
		return nil, err
	}
	var size int
	if info, err := r.File.Stat(); err == nil {
		size64 := info.Size()
		if int64(int(size64)) == size64 {
			size = int(size64)
		}
	}
	size++
	if size < 512 {
		size = 512
	}

	data := make([]byte, 0, size)
	for {
		if len(data) >= cap(data) {
			d := append(data[:cap(data)], 0)
			data = d[:len(data)]
		}
		n, err := r.File.Read(data[len(data):cap(data)])
		data = data[:len(data)+n]
		if err != nil {
			if err == io.EOF {
				err = nil
			}
			break
		}
	}
	if err != nil {
		return nil, err
	}

	res := bytes.Split(bytes.TrimSpace(data), []byte{'\n'})
	if res[0] == nil {

		return nil, nil
	}
	s := make([]interface{}, len(res))
	for i, v := range res {
		tempField := string(v)
		fields := strings.Split(tempField, ",")

		asd, err := strconv.Atoi(strings.Trim(strings.Split(fields[0], ":")[1], "\""))
		if err != nil {
			return nil, err
		}

		dsa := strings.Trim(strings.Split(fields[1], "\":\"")[1], "\"}\n")
		user := User{
			ID:   asd,
			Name: dsa,
		}
		s[i] = user

	}

	return s, err
}

func ExistFile(filePath string) error {
	if _, err := os.Stat(filePath); err != nil {
		if os.IsNotExist(err) {
			// file does not exist
			f, err := os.Create(filePath)
			if err != nil {
				return err
			}
			err = f.Close()
			if err != nil {
				return err
			}

		} else {
			// other error
			return err
		}
	}
	return nil
}
