package task

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"time"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	f, err := os.ReadFile(path)
	if err != nil {
		return err
	}

	var commitsSort []Commit
	if err = json.Unmarshal(f, &commitsSort); err != nil {
		return err
	}

	// отсортировать список используя самописный quickSort
	commitsSort = quickSort(commitsSort)
	createLinkedList(d, commitsSort)
	return nil
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.Current() == nil {
		return nil
	}
	return d.curr.next
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.Current() == nil {
		return nil
	}
	return d.curr.prev
}

func linkNode(d *DoubleLinkedList, nodeLeft, nodeInsert, nodeRight *Node) error {
	if nodeInsert == nil && nodeLeft == nil || nodeInsert == nil && nodeRight == nil {
		return errors.New("node insert non`t be nil")
	}
	if nodeLeft == nil {
		d.head = nodeInsert
		nodeRight.prev = nodeInsert
		d.head.next = nodeRight
		d.head.prev = nil

	} else if nodeRight == nil {
		d.tail = nodeInsert
		nodeLeft.next = nodeInsert
		d.tail.prev = nodeLeft
		d.tail.next = nil

	} else if nodeInsert == nil { //  ToDo: Delete case?
		nodeLeft.next, nodeRight.prev = nodeRight, nodeLeft
	} else {
		nodeLeft.next = nodeInsert
		nodeInsert.prev = nodeLeft
		nodeInsert.next = nodeRight
		nodeRight.prev = nodeInsert
	}
	return nil
}

// Insert вставка элемента после del элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	nodeInsert := &Node{data: &c}
	if d.len < n {
		return fmt.Errorf("element - %v does not exist", n)
	}

	if n == d.Len() {
		if err := linkNode(d, d.tail, nodeInsert, nil); err != nil {
			return err
		}

	} else {
		nodePast := nodeForIndex(d, n)
		if err := linkNode(d, nodePast, nodeInsert, nodePast.next); err != nil {
			return err
		}
	}

	d.len++
	return nil
}

func nodeForIndex(d *DoubleLinkedList, n int) *Node {
	currNode := d.head
	for i := 0; i != n; i++ {
		currNode = currNode.next
	}
	return currNode
}

// Delete удаление del элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if d.len < n+1 {
		return fmt.Errorf("element - %v does not exist", n)
	}
	if n < 0 {
		return errors.New("negative index")
	}

	if n == 0 {
		if d.head == d.curr {
			d.curr = newCurrentNode(d)
		}
		d.head = d.head.next
		d.head.prev = nil
	} else if n == d.Len()-1 {
		if d.tail == d.curr {
			d.curr = newCurrentNode(d)
		}
		d.tail = d.tail.prev
		d.tail.next = nil
	} else {
		nodeDelete := nodeForIndex(d, n)
		if nodeDelete == d.curr {
			d.curr = newCurrentNode(d)
		}
		if err := linkNode(d, nodeDelete.prev, nil, nodeDelete.next); err != nil {
			return err
		}
	}

	d.len--

	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.len == 0 {
		return errors.New("double linked list is empty")
	}
	if d.curr == nil {
		return errors.New("don`t delete nil node")
	}

	if d.curr == d.head {
		d.curr = newCurrentNode(d)
		d.head = d.head.next
		d.head.prev = nil
	} else if d.curr == d.tail {
		d.curr = newCurrentNode(d)
		d.tail = d.tail.prev
		d.tail.next = nil
	} else {

		futureCurr := newCurrentNode(d)
		if err := linkNode(d, d.curr.prev, nil, d.curr.next); err != nil {
			return err
		}
		d.curr = futureCurr
	}

	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.len == 0 {
		return 0, errors.New("double linked list is empty")
	}
	if d.curr == nil {
		return 0, errors.New("don`t index nil node")
	}
	var index int
	tempNode := d.head
	for tempNode.data != d.curr.data {
		tempNode = tempNode.next
		index++
	}

	return index, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.Len() == 0 {
		return nil
	}
	if d.curr == d.tail {
		d.curr = newCurrentNode(d)
	}
	res := d.tail
	linkNode(d, d.tail.prev.prev, d.tail.prev, nil)
	d.len--
	return res
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.Len() == 0 {
		return nil
	}
	if d.curr == d.head {
		d.curr = newCurrentNode(d)
	}
	res := d.head
	linkNode(d, nil, d.head.next, d.head.next.next)
	d.len--
	return res
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	temp := d.head
	for {
		if temp.data.UUID == uuID {
			return temp
		}
		if temp.next == nil {
			break
		}
		temp = temp.next
	}

	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	temp := d.head
	for {
		if temp.data.Message == message {
			break
		}
		if temp.next == nil {
			return nil
		}
		temp = temp.next
	}

	return temp
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	if d.len <= 1 {
		return d
	}
	d.head, d.tail = d.tail, d.head
	newTail := d.tail
	for i := 0; i < d.len; i++ {
		temp := d.tail.next
		d.tail.next, d.tail.prev = d.tail.prev, d.tail.next
		d.tail = temp
	}
	d.tail = newTail

	return d
}

func newCurrentNode(d *DoubleLinkedList) *Node {
	if d.curr.prev != nil {
		return d.curr.prev
	} else if d.curr.next != nil {
		return d.curr.next
	} else if d.head != nil {
		d.curr = d.head
	}
	return nil
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON() {
	// Дополнительное задание написать генератор данных
	// используя библиотеку gofakeit
}

func quickSortVariant(arr []Commit, low, high int) []Commit {
	if low < high {
		var p int
		arr, p = partition(arr, low, high)
		arr = quickSortVariant(arr, low, p-1)
		arr = quickSortVariant(arr, p+1, high)
	}
	return arr
}

func quickSortStart(arr []Commit) []Commit {
	return quickSortVariant(arr, 0, len(arr)-1)
}

func partition(arr []Commit, low, high int) ([]Commit, int) {
	pivot := arr[high].Date
	i := low
	for j := low; j < high; j++ {
		if arr[j].Date.Before(pivot) {
			arr[i], arr[j] = arr[j], arr[i]
			i++
		}
	}
	arr[i], arr[high] = arr[high], arr[i]
	return arr, i
}

func createLinkedList(d *DoubleLinkedList, commits []Commit) {
	nodeFirst := &Node{
		data: &commits[0],
		prev: nil,
		next: nil,
	}
	d.head, d.curr = nodeFirst, nodeFirst

	nodePast := nodeFirst
	for i := 1; i < len(commits); i++ {
		nodeCurrent := &Node{
			data: &commits[i],
			prev: nodePast,
			next: nil,
		}

		nodePast.next = nodeCurrent
		nodePast = nodePast.next
	}
	d.tail = nodePast

	d.len = len(commits)
}

func quickSort(arr []Commit) []Commit {

	if len(arr) <= 1 {
		return arr
	}

	median := arr[rand.Intn(len(arr))].Date

	lowPart := make([]Commit, 0, len(arr))
	highPart := make([]Commit, 0, len(arr))
	middlePart := make([]Commit, 0, len(arr))

	for _, item := range arr {
		switch {
		case median.After(item.Date):
			lowPart = append(lowPart, item)
		case median.Equal(item.Date):
			middlePart = append(middlePart, item)
		case median.Before(item.Date):
			highPart = append(highPart, item)

		}
	}
	lowPart = quickSort(lowPart)
	highPart = quickSort(highPart)

	lowPart = append(lowPart, middlePart...)
	lowPart = append(lowPart, highPart...)

	return lowPart
}
