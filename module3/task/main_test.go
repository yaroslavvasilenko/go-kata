package task

import (
	"encoding/json"
	"log"
	"os"
	"reflect"
	"sort"
	"strconv"
	"testing"
	"time"
)

var theTime = time.Date(2021, 8, 15, 14, 30, 45, 100, time.Local)
var (
	Commit1 = Commit{
		Message: "first",
		UUID:    "1",
		Date:    theTime.Add(time.Second * 1),
	}
	Commit2 = Commit{
		Message: "second",
		UUID:    "2",
		Date:    theTime.Add(time.Second * 2),
	}
	Commit3 = Commit{
		Message: "third",
		UUID:    "3",
		Date:    theTime.Add(time.Second * 3),
	}
	Commit4 = Commit{
		Message: "fourth",
		UUID:    "4",
		Date:    theTime.Add(time.Second * 4),
	}
	Commit5 = Commit{
		Message: "fifth",
		UUID:    "5",
		Date:    theTime.Add(time.Second * 5),
	}
	CommitMock = Commit{}
)

var commitsNoSort = []Commit{
	Commit4,
	Commit1,
	Commit3,
	Commit5,
	Commit2,
}

func createLinkedListTest(c []Commit, currenNode int) *DoubleLinkedList {
	d := &DoubleLinkedList{}
	if len(c) == 0 {
		return d
	}
	createLinkedList(d, c)
	d.curr = nodeForIndex(d, currenNode)
	return d
}

func createLinkedListTestHard(c []Commit) *DoubleLinkedList {

	nodeArr := make([]Node, len(c))
	nodeArr[0].data = &c[0]
	nodeArr[0].prev = nil
	nodeArr[0].next = &nodeArr[1]

	nodeArr[1].data = &c[1]
	nodeArr[1].prev = &nodeArr[0]
	nodeArr[1].next = &nodeArr[2]

	nodeArr[2].data = &c[2]
	nodeArr[2].prev = &nodeArr[1]
	nodeArr[2].next = &nodeArr[3]

	nodeArr[3].data = &c[3]
	nodeArr[3].prev = &nodeArr[2]
	nodeArr[3].next = &nodeArr[4]

	nodeArr[4].data = &c[4]
	nodeArr[4].prev = &nodeArr[3]
	nodeArr[4].next = nil

	d := &DoubleLinkedList{
		head: &nodeArr[0],
		tail: &nodeArr[4],
		curr: &nodeArr[0],
		len:  5,
	}

	return d
}

func Test_quickSortStart(t *testing.T) {
	commitsSort := commitsSortTest()
	type args struct {
		arr []Commit
	}
	tests := []struct {
		name string
		args args
		want []Commit
	}{
		// TODO: Add commitsSort cases.
		{name: "no sort",
			args: args{commitsNoSort},
			want: commitsSort,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := quickSortStart(tt.args.arr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("quickSortStart() = %v, \n want %v", got, tt.want)
			}
		})
	}
}

func commitsSortTest() []Commit {
	var commitsSort = make([]Commit, len(commitsNoSort))
	copy(commitsSort, commitsNoSort)
	sort.Slice(commitsSort, func(i, j int) bool {
		a, _ := strconv.Atoi(commitsSort[i].UUID)
		b, _ := strconv.Atoi(commitsSort[j].UUID)
		return a < b
	})
	return commitsSort
}

func Test_createLinkedList(t *testing.T) {
	type args struct {
		d       *DoubleLinkedList
		commits []Commit
	}
	commitsSort := commitsSortTest()
	tests := []struct {
		name string
		args args
		want *DoubleLinkedList
	}{
		// TODO: Add test cases.
		{
			name: "right linked list",
			args: args{
				d:       &DoubleLinkedList{},
				commits: commitsSort,
			},
			want: createLinkedListTestHard(commitsSortTest()),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			createLinkedList(tt.args.d, tt.args.commits)
			if !reflect.DeepEqual(tt.args.d, tt.want) {
				t.Errorf("quickSortStart() = %v, \n want %v", tt.args.d, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Len(t *testing.T) {

	tests := []struct {
		name   string
		fields *DoubleLinkedList
		want   int
	}{
		// TODO: Add test cases.
		{
			name:   "len 5",
			fields: &DoubleLinkedList{len: 5},
			want:   5,
		},
		{
			name:   "len 0",
			fields: &DoubleLinkedList{len: 0},
			want:   0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Len(); got != tt.want {
				t.Errorf("Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Current(t *testing.T) {

	tests := []struct {
		name   string
		fields *DoubleLinkedList
		want   *Node
	}{
		// TODO: Add test cases.
		{
			name:   "[0]current node",
			fields: createLinkedListTestHard(commitsSortTest()),
			want:   createLinkedListTestHard(commitsSortTest()).curr,
		},
		{
			name:   "nil current node",
			fields: &DoubleLinkedList{},
			want:   nil,
		},
		{
			name:   "[4]current node",
			fields: createLinkedListTest(commitsSortTest(), 4),
			want:   createLinkedListTest(commitsSortTest(), 4).curr,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Current(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Current() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Next(t *testing.T) {
	data := createLinkedListTestHard(commitsSortTest())
	data.curr = nodeForIndex(data, 4)
	tests := []struct {
		name   string
		fields *DoubleLinkedList
		want   *Node
	}{
		// TODO: Add test cases.
		{
			name:   "[1]next node",
			fields: createLinkedListTestHard(commitsSortTest()),
			want:   createLinkedListTestHard(commitsSortTest()).curr.next,
		},
		{
			name:   "[5]next node = nil",
			fields: data,
			want:   data.curr.next,
		},
		{
			name:   "current node is nil",
			fields: &DoubleLinkedList{},
			want:   nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Next(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Next() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Prev(t *testing.T) {
	tests := []struct {
		name   string
		fields *DoubleLinkedList
		want   *Node
	}{
		// TODO: Add test cases.
		{
			name:   "[1]prev node is nil",
			fields: createLinkedListTestHard(commitsSortTest()),
			want:   createLinkedListTestHard(commitsSortTest()).curr.prev,
		},
		{
			name:   "[5]prev node ",
			fields: createLinkedListTest(commitsSortTest(), 4),
			want:   createLinkedListTest(commitsSortTest(), 4).curr.prev,
		},
		{
			name:   "current node is nil",
			fields: &DoubleLinkedList{},
			want:   nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Prev(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Prev() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_LoadData(t *testing.T) {

	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name:    "load data right",
			args:    args{path: "./test.json"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{}
			if err := d.LoadData(tt.args.path); (err != nil) != tt.wantErr {
				t.Errorf("LoadData() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	type args struct {
		n int
		c Commit
	}

	tests := []struct {
		name    string
		fields  *DoubleLinkedList
		args    args
		want    *DoubleLinkedList
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name:   "insert for index 2",
			fields: createLinkedListTest(commitsSortTest(), 0),
			args: args{
				n: 2,
				c: CommitMock,
			},
			want: createLinkedListTest([]Commit{Commit1, Commit2, Commit3, CommitMock, Commit4, Commit5}, 0),
		},
		{
			name:   "insert for last index",
			fields: createLinkedListTest(commitsSortTest(), 0),
			args: args{
				n: 4,
				c: CommitMock,
			},
			want: createLinkedListTest([]Commit{Commit1, Commit2, Commit3, Commit4, Commit5, CommitMock}, 0),
		},
		{
			name:   "insert for first index",
			fields: createLinkedListTest(commitsSortTest(), 0),
			args: args{
				n: 0,
				c: CommitMock,
			},
			want: createLinkedListTest([]Commit{Commit1, CommitMock, Commit2, Commit3, Commit4, Commit5}, 0),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := tt.fields.Insert(tt.args.n, tt.args.c)
			if (err != nil) != tt.wantErr {
				t.Errorf("Insert() error = %v, wantErr %v", err, tt.wantErr)

			}

			if !reflect.DeepEqual(tt.fields, tt.want) {
				t.Errorf("quickSortStart() = %v, \n want %v", tt.fields, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Delete(t *testing.T) {
	type args struct {
		del int
	}
	tests := []struct {
		name    string
		fields  *DoubleLinkedList
		args    args
		want    *DoubleLinkedList
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name:    "delete first elem node list ",
			fields:  createLinkedListTest(commitsSortTest(), 0),
			args:    args{0},
			want:    createLinkedListTest([]Commit{Commit2, Commit3, Commit4, Commit5}, 0),
			wantErr: false,
		},
		{
			name:    "delete last elem node list",
			fields:  createLinkedListTest(commitsSortTest(), 4),
			args:    args{4},
			want:    createLinkedListTest([]Commit{Commit1, Commit2, Commit3, Commit4}, 3),
			wantErr: false,
		},
		{
			name:    "delete [3]node list",
			fields:  createLinkedListTest(commitsSortTest(), 3),
			args:    args{3},
			want:    createLinkedListTest([]Commit{Commit1, Commit2, Commit3, Commit5}, 2),
			wantErr: false,
		},
		{
			name:    "delete - out of range array",
			fields:  createLinkedListTest(commitsSortTest(), 0),
			args:    args{5},
			want:    createLinkedListTest(commitsSortTest(), 0),
			wantErr: true,
		},
		{
			name:    "delete - out of range array",
			fields:  createLinkedListTest(commitsSortTest(), 0),
			args:    args{-1},
			want:    createLinkedListTest(commitsSortTest(), 0),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			err := d.Delete(tt.args.del)

			if (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(d, tt.want) {
				t.Errorf("Delete() = %v, \n want %v", d, tt.want)
			}

		})
	}
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	tests := []struct {
		name   string
		fields *DoubleLinkedList

		want    *DoubleLinkedList
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name:    "delete current first elem node list ",
			fields:  createLinkedListTest(commitsSortTest(), 0),
			want:    createLinkedListTest([]Commit{Commit2, Commit3, Commit4, Commit5}, 0),
			wantErr: false,
		},
		{
			name:    "delete current last elem node list",
			fields:  createLinkedListTest(commitsSortTest(), 4),
			want:    createLinkedListTest([]Commit{Commit1, Commit2, Commit3, Commit4}, 3),
			wantErr: false,
		},
		{
			name:    "delete current [3]node list",
			fields:  createLinkedListTest(commitsSortTest(), 3),
			want:    createLinkedListTest([]Commit{Commit1, Commit2, Commit3, Commit5}, 2),
			wantErr: false,
		},
		{
			name: "delete current - nil current node",
			fields: func() *DoubleLinkedList {
				d := createLinkedListTest(commitsSortTest(), 0)
				d.curr = nil
				return d
			}(),
			want: func() (d *DoubleLinkedList) {
				d = createLinkedListTest(commitsSortTest(), 0)
				d.curr = nil
				return
			}(),
			wantErr: true,
		},
		{
			name:    "delete current - empty linked list",
			fields:  createLinkedListTest([]Commit{}, 0),
			want:    createLinkedListTest([]Commit{}, 0),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.DeleteCurrent(); (err != nil) != tt.wantErr {
				t.Errorf("DeleteCurrent() error = %+v\n, \n want %+v\n", err, tt.wantErr)
			}

			if !reflect.DeepEqual(d, tt.want) {
				t.Errorf("Delete() = %+v\n, \n want %+v\n", d, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Pop(t *testing.T) {
	type wantFields struct {
		linkedList *DoubleLinkedList
		node       *Node
	}
	tests := []struct {
		name   string
		fields *DoubleLinkedList
		want   *wantFields
	}{
		// TODO: Add test cases.
		{
			name:   "pop - last curr node",
			fields: createLinkedListTest(commitsSortTest(), 4),
			want: &wantFields{
				linkedList: createLinkedListTest([]Commit{Commit1, Commit2, Commit3, Commit4}, 3),
				node:       createLinkedListTest(commitsSortTest(), 4).curr,
			},
		},
		{
			name:   "pop - nil linked list",
			fields: createLinkedListTest([]Commit{}, 0),
			want: &wantFields{
				linkedList: createLinkedListTest([]Commit{}, 0),
				node:       nil,
			},
		},
		{
			name:   "pop",
			fields: createLinkedListTest(commitsSortTest(), 0),
			want: &wantFields{
				linkedList: createLinkedListTest([]Commit{Commit1, Commit2, Commit3, Commit4}, 0),
				node:       createLinkedListTest(commitsSortTest(), 4).curr,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			got := d.Pop()
			if got == nil && !reflect.DeepEqual(got, tt.want.node) {
				t.Errorf("Pop() = %v, want %v", got, tt.want.node)
			}
			if got != nil && !reflect.DeepEqual(got.data, tt.want.node.data) {
				t.Errorf("Pop() = %v, want %v", got.data, tt.want.node.data)
			}
			if !reflect.DeepEqual(d, tt.want.linkedList) {
				t.Errorf("Pop() = %v, want %v", d, tt.want.linkedList)
			}
		})
	}
}

func TestDoubleLinkedList_Shift(t *testing.T) {
	type wantFields struct {
		linkedList *DoubleLinkedList
		node       *Node
	}
	tests := []struct {
		name   string
		fields *DoubleLinkedList
		want   *wantFields
	}{
		// TODO: Add test cases.
		{
			name:   "shift - first curr node",
			fields: createLinkedListTest(commitsSortTest(), 0),
			want: &wantFields{
				linkedList: createLinkedListTest([]Commit{Commit2, Commit3, Commit4, Commit5}, 0),
				node:       createLinkedListTest(commitsSortTest(), 0).curr,
			},
		},
		{
			name:   "shift - nil linked list",
			fields: createLinkedListTest([]Commit{}, 0),
			want: &wantFields{
				linkedList: createLinkedListTest([]Commit{}, 0),
				node:       nil,
			},
		},
		{name: "shift",
			fields: createLinkedListTest(commitsSortTest(), 3),
			want: &wantFields{
				linkedList: createLinkedListTest([]Commit{Commit2, Commit3, Commit4, Commit5}, 2),
				node:       createLinkedListTest(commitsSortTest(), 0).curr,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			got := d.Shift()
			if got == nil && !reflect.DeepEqual(got, tt.want.node) {
				t.Errorf("Shift() = %v, want %v", got, tt.want.node)
			}
			if got != nil && !reflect.DeepEqual(got.data, tt.want.node.data) {
				t.Errorf("Shift() = %v, want %v", got.data, tt.want.node.data)
			}
			if !reflect.DeepEqual(d, tt.want.linkedList) {
				t.Errorf("Shift() = %v, want %v", d, tt.want.linkedList)
			}
		})
	}
}

func TestDoubleLinkedList_SearchUUID(t *testing.T) {
	type args struct {
		uuID string
	}
	tests := []struct {
		name   string
		fields *DoubleLinkedList
		args   args
		want   *Node
	}{
		// TODO: Add test cases.
		{
			name:   "search uuid - 1",
			fields: createLinkedListTest(commitsSortTest(), 0),
			args:   args{uuID: "1"},
			want:   createLinkedListTest(commitsSortTest(), 0).curr,
		},
		{
			name:   "search uuid - 5",
			fields: createLinkedListTest(commitsSortTest(), 0),
			args:   args{uuID: "5"},
			want:   createLinkedListTest(commitsSortTest(), 4).curr,
		},
		{
			name:   "search uuid - 3",
			fields: createLinkedListTest(commitsSortTest(), 0),
			args:   args{uuID: "3"},
			want:   createLinkedListTest(commitsSortTest(), 2).curr,
		},
		{
			name:   "search uuid - 9 (not available)",
			fields: createLinkedListTest(commitsSortTest(), 0),
			args:   args{uuID: "9"},
			want:   nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.SearchUUID(tt.args.uuID); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SearchUUID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Search(t *testing.T) {
	type args struct {
		message string
	}
	tests := []struct {
		name   string
		fields *DoubleLinkedList
		args   args
		want   *Node
	}{
		// TODO: Add test cases.
		{
			name:   "search uuid - 1",
			fields: createLinkedListTest(commitsSortTest(), 0),
			args:   args{message: "first"},
			want:   createLinkedListTest(commitsSortTest(), 0).curr,
		},
		{
			name:   "search uuid - 5",
			fields: createLinkedListTest(commitsSortTest(), 0),
			args:   args{message: "fifth"},
			want:   createLinkedListTest(commitsSortTest(), 4).curr,
		},
		{
			name:   "search uuid - 3",
			fields: createLinkedListTest(commitsSortTest(), 0),
			args:   args{message: "third"},
			want:   createLinkedListTest(commitsSortTest(), 2).curr,
		},
		{
			name:   "search uuid - 9 (not available)",
			fields: createLinkedListTest(commitsSortTest(), 0),
			args:   args{message: "nine"},
			want:   nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Search(tt.args.message); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Search() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleLinkedList_Reverse(t *testing.T) {
	tests := []struct {
		name string
		args *DoubleLinkedList
		want *DoubleLinkedList
	}{
		// TODO: Add test cases.
		{
			name: "regular reverse linked list",
			args: createLinkedListTest(commitsSortTest(), 0),
			want: createLinkedListTest([]Commit{Commit5, Commit4, Commit3, Commit2, Commit1}, 4),
		},
		{
			name: "nil linked list",
			args: createLinkedListTest([]Commit{}, 0),
			want: createLinkedListTest([]Commit{}, 0),
		},
		{
			name: "nil linked list",
			args: createLinkedListTest([]Commit{Commit1}, 0),
			want: createLinkedListTest([]Commit{Commit1}, 0),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.args.Reverse()
			if !reflect.DeepEqual(tt.args, tt.want) {
				t.Errorf("quickSortStart() = %v, \n want %v", tt.args, tt.want)
			}
		})
	}
}

func Benchmark_quickSortStart(b *testing.B) {

	f, err := os.ReadFile("./test.json")
	if err != nil {
		log.Println(err)
		return
	}

	var commitsSort []Commit
	if err = json.Unmarshal(f, &commitsSort); err != nil {
		log.Println(err)
		return
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		commitsSort = quickSortStart(commitsSort)
	}

}

func Benchmark_QuickSort(b *testing.B) {

	f, err := os.ReadFile("./test.json")
	if err != nil {
		log.Println(err)
		return
	}

	var commitsSort []Commit
	if err = json.Unmarshal(f, &commitsSort); err != nil {
		log.Println(err)
		return
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		commitsSort = quickSort(commitsSort)
	}

}

func Benchmark_QuickSort3(b *testing.B) {

	f, err := os.ReadFile("./test.json")
	if err != nil {
		log.Println(err)
		return
	}

	var commitsSort []Commit
	if err = json.Unmarshal(f, &commitsSort); err != nil {
		log.Println(err)
		return
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		sort.Slice(commitsSort, func(i, j int) bool {
			return commitsSort[i].Date.Before(commitsSort[j].Date)
		})
	}
}

func Benchmark_QuickSort2(b *testing.B) {

	f, err := os.ReadFile("./test.json")
	if err != nil {
		log.Println(err)
		return
	}

	var commitsSort []Commit
	if err = json.Unmarshal(f, &commitsSort); err != nil {
		log.Println(err)
		return
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		sort.SliceStable(commitsSort, func(i, j int) bool {
			return commitsSort[i].Date.Before(commitsSort[j].Date)
		})
	}
}
