package main

import (
	"fmt"
	"reflect"
)

var (
	crasySale  float64 = 0.5
	springCall float64 = 0.2
	luck       float64 = 0.1
)

type PricingStrategy interface {
	calculate(o *order) float64
	whoAreYou() string
}

type regularPricing struct {
}

type salePricing struct {
	discount float64
}

type order struct {
	title  string
	price  int
	amount int
}

func (p *regularPricing) calculate(o *order) float64 {
	return float64(o.price * o.amount)
}

func (p *salePricing) calculate(o *order) float64 {
	return float64(o.price*o.amount) * p.discount
}

func main() {
	arr := []PricingStrategy{&regularPricing{},
		&salePricing{discount: crasySale}, &salePricing{discount: springCall}, &salePricing{discount: luck}}
	o := &order{
		title:  "phone",
		price:  200,
		amount: 3,
	}
	for _, obj := range arr {
		fmt.Printf("%s strategy: %.2f cost \n", obj.whoAreYou(), obj.calculate(o))
	}
}

func (p *salePricing) whoAreYou() string {
	return reflect.TypeOf(*p).Name()
}

func (p *regularPricing) whoAreYou() string {
	return reflect.TypeOf(*p).Name()
}
