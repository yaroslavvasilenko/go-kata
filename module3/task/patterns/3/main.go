package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

// WeatherAPI is the interface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature(location string) int
	GetHumidity(location string) int
	GetWindSpeed(location string) int
}

type weather struct {
	Coord struct {
		Lon float64 `json:"lon"`
		Lat float64 `json:"lat"`
	} `json:"coord"`
	Weather []struct {
		ID          int    `json:"id"`
		Main        string `json:"main"`
		Description string `json:"description"`
		Icon        string `json:"icon"`
	} `json:"weather"`
	Base string `json:"base"`
	Main struct {
		Temp      float64 `json:"temp"`
		FeelsLike float64 `json:"feels_like"`
		TempMin   float64 `json:"temp_min"`
		TempMax   float64 `json:"temp_max"`
		Pressure  int     `json:"pressure"`
		Humidity  int     `json:"humidity"`
	} `json:"main"`
	Visibility int `json:"visibility"`
	Wind       struct {
		Speed float64 `json:"speed"`
		Deg   int     `json:"deg"`
	} `json:"wind"`
	Clouds struct {
		All int `json:"all"`
	} `json:"clouds"`
	Dt  int `json:"dt"`
	Sys struct {
		Type    int    `json:"type"`
		ID      int    `json:"id"`
		Country string `json:"country"`
		Sunrise int    `json:"sunrise"`
		Sunset  int    `json:"sunset"`
	} `json:"sys"`
	Timezone int    `json:"timezone"`
	ID       int    `json:"id"`
	Name     string `json:"name"`
	Cod      int    `json:"cod"`
}

type cash struct {
	w *weather
	t time.Time
}

var cacheWeather struct {
	m map[string]cash
}

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey string
}

func (o *OpenWeatherAPI) GetTemperature(location string) int {

	w := o.weatherCall(location)

	return int(w.Main.Temp)
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	w := o.weatherCall(location)

	return w.Main.Humidity
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) int {
	w := o.weatherCall(location)

	return int(w.Wind.Speed)
}

func (o *OpenWeatherAPI) weatherCall(location string) *weather {
	v, ok := cacheWeather.m[location]
	if ok {
		if v.t.Before(time.Now().Add(time.Hour)) {
			return v.w
		}

	}
	url := "https://api.openweathermap.org/data/2.5/weather?q=" + location + "&appid=" + o.apiKey
	resp, err := http.Get(url)
	if err != nil {
		log.Println(err)
		return nil
	}
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return nil
	}
	weath := &weather{}
	err = json.Unmarshal(data, weath)
	if err != nil {
		return nil
	}

	cacheWeather.m[location] = cash{
		w: weath,
		t: time.Now(),
	}
	return weath
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int) {
	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey},
	}
}

const apiKey = "b940270f393bb87a261dbaf949cd6b47"

func main() {

	cacheWeather.m = make(map[string]cash, 0)
	weatherFacade := NewWeatherFacade(apiKey)
	cities := []string{"Moscow", "Saint Petersburg", "Kazan", "Yakutsk"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d\n\n", windSpeed)
	}
}
