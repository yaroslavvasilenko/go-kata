package main

import "fmt"

type AirConditioner interface {
	changeTemperature(t int)
	powerOff()
	powerOn()
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	return &AirConditionerProxy{
		adapter: &AirConditionerAdapter{
			airConditioner: &RealAirConditioner{
				temperature: 20,
				power:       false,
			}},
		authenticated: authenticated,
	}
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

type RealAirConditioner struct {
	temperature int
	power       bool
}

func main() {
	cond := NewAirConditionerProxy(false)
	cond.powerOnProxy()
	cond.powerOffProxy()
	cond.changeTemperatureProxy(-5)

	cond = NewAirConditionerProxy(true)
	cond.powerOnProxy()
	cond.powerOffProxy()
	cond.changeTemperatureProxy(5)

}

func (p *AirConditionerProxy) changeTemperatureProxy(t int) {
	if p.authenticated {
		p.adapter.changeTemperatureAdapter(t)
	} else {
		fmt.Println("Access denied: authentication required to set the temperature of the air conditioner")
	}
}

func (p *AirConditionerProxy) powerOffProxy() {
	if p.authenticated {
		p.adapter.powerOffAdapter()
	} else {
		fmt.Println("Access denied: authentication required to turn off the air conditioner")
	}
}

func (p *AirConditionerProxy) powerOnProxy() {
	if p.authenticated {
		p.adapter.powerOnAdapter()
	} else {
		fmt.Println("Access denied: authentication required to turn on the air conditioner")
	}
}

func (a *AirConditionerAdapter) changeTemperatureAdapter(t int) {
	a.airConditioner.changeTemperature(t)
}

func (a *AirConditionerAdapter) powerOffAdapter() {
	a.airConditioner.powerOff()
}

func (a *AirConditionerAdapter) powerOnAdapter() {
	a.airConditioner.powerOn()
}

func (r *RealAirConditioner) changeTemperature(t int) {
	changeT := r.temperature + t
	if 16 > changeT {
		return
	}

	if changeT > 32 {
		return
	}
	r.temperature = changeT
	fmt.Printf("Setting air conditioner temperature to %v", r.temperature)
}

func (r *RealAirConditioner) powerOff() {
	r.power = false
	fmt.Println("Turning off the air conditioner")
}

func (r *RealAirConditioner) powerOn() {
	r.power = true
	fmt.Println("Turning on the air conditioner")
}
