package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const closeChanel = time.Second * 30

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData(ctx context.Context) chan int {
	out := make(chan int, 1000)

	go func() {
		for {
			select {
			case <-ctx.Done():
				close(out)
				return
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	ctx, cancel := context.WithTimeout(context.Background(), closeChanel)

	out := generateData(ctx)

	go func() {
		for num := range out {
			a <- num
		}
		close(a)
	}()

	go func() {
		for num := range out {
			b <- num
		}
		close(b)
	}()

	go func() {
		for num := range out {
			c <- num
		}
		close(c)
	}()

	mainChan := joinChannels(a, b, c)

	for num := range mainChan {
		fmt.Println(num)
	}

	time.Sleep(3 * time.Second)
	cancel()

}
