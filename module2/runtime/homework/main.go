// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Println("i can manage")
	go func() {
		runtime.Gosched()
		fmt.Println("goroutines in Golang!")
	}()
	fmt.Println("and its awesome!")
}
