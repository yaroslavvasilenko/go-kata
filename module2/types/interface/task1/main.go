// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r MyInterface) {
	switch v := r.(type) {
	case *int:
		if v == nil {
			fmt.Println("Success!")
		} else {
			fmt.Println("*int != nil")
		}
	case nil:
		fmt.Println("interface = nil")
	default:
		fmt.Println("defeat")
	}
}
