package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  36,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}

	newUsers := make([]User, 0, 7)
	var lenUsers int = len(users)
	for i := 0; i < lenUsers; i++ {
		if users[i].Age < 40 {
			newUsers = append(newUsers, users[i])
		}
	}
	users = newUsers

	fmt.Println(users)
}
