package main

import "fmt"

func main() {
	// 1 option
	s := []int{1, 2, 3}
	s = Append(s)
	fmt.Println(s)

	// 2 option
	s2 := []int{1, 2, 3}
	Append2(&s2)
	fmt.Println(s2)
}

func Append(s []int) []int {
	return append(s, 4)

}

func Append2(s *[]int) {
	*s = append(*s, 4)
}
