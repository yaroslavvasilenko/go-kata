package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/rs/zerolog",
			Stars: 79000,
		},
		{
			Name:  "https://github.com/sourcegraph/conc",
			Stars: 6400,
		},
		{
			Name:  "https://github.com/jackc/pgx",
			Stars: 6900,
		},
		{
			Name:  "https://github.com/pressly/goose",
			Stars: 3700,
		},
		{
			Name:  "https://github.com/avelino/awesome-go",
			Stars: 98500,
		},
		{
			Name:  "https://github.com/grpc/grpc-go",
			Stars: 17800,
		},
		{
			Name:  "https://github.com/go-gorm/gorm",
			Stars: 31900,
		},
		{
			Name:  "https://github.com/go-telegram/bot",
			Stars: 58,
		},
		{
			Name:  "https://github.com/docker/cli",
			Stars: 3900,
		},
		{
			Name:  "https://github.com/sahilm/fuzzy",
			Stars: 1200,
		},
		{
			Name:  "https://github.com/go-mgo/mgo",
			Stars: 2700,
		},
		{
			Name:  "https://github.com/tsenart/vegeta",
			Stars: 21000,
		},
		// сюда впишите ваши остальные 12 структур
	}

	// в цикле запишите в map
	mapProjects := make(map[string]Project, 13)

	for i := 0; i < len(projects); i++ {
		mapProjects[projects[i].Name] = projects[i]
	}

	// в цикле пройдитесь по мапе и выведите значения в консоль
	for _, value := range mapProjects {
		fmt.Println(value)
	}
}
