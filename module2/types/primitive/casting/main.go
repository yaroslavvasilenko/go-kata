package main

import "fmt"

func main() {
	var numberFloat float32 = 3

	fmt.Printf("тип: %T, значение: %v\n", numberFloat, numberFloat)

	numberInt := int(numberFloat)

	fmt.Printf("тип: %T, значение: %v", numberInt, numberInt)
}
