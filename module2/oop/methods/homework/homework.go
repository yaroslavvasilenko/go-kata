package main

import "fmt"

type calc struct {
	a, b   float64
	result float64
}

func newCalc() *calc { // конструктор калькулятора
	return &calc{}
}

func (c *calc) SetA(a float64) {
	c.a = a
}

func (c *calc) SetB(b float64) {
	c.b = b
}

func (c *calc) Do(operation func(a, b float64) float64) {
	c.result = operation(c.a, c.b)
}

func (c *calc) Result() float64 {
	return c.result
}

func multiply(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a * b
}
func divide(a, b float64) float64 {
	return a / b
}

func sum(a, b float64) float64 {
	return a + b
}

func average(a, b float64) float64 {
	return (a + b) / 2
}

func main() {
	calc := newCalc()
	calc.SetA(10)
	calc.SetB(24)

	calc.Do(multiply)
	res := calc.Result()
	fmt.Println(res)

	res2 := calc.Result()
	fmt.Println(res2)
	if res != res2 {
		panic("object statement is not persist")
	}
}
