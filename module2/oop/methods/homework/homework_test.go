package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_multiply(t *testing.T) {
	testCalc := [8]struct {
		name   string
		a      float64
		b      float64
		result float64
	}{
		{
			name:   "valid case 1",
			a:      2,
			b:      2,
			result: 4,
		},
		{
			name:   "valid case 2",
			a:      2,
			b:      0,
			result: 0,
		},
		{
			name:   "valid case 3",
			a:      0,
			b:      2,
			result: 0,
		},
		{
			name:   "valid case 4",
			a:      0,
			b:      0,
			result: 0,
		},
		{
			name:   "valid case 5",
			a:      -5,
			b:      2,
			result: -10,
		},
		{
			name:   "valid case 6",
			a:      100,
			b:      100,
			result: 10000,
		},
		{
			name:   "valid case 7",
			a:      -2,
			b:      -2,
			result: 4,
		},
		{
			name:   "valid case 8",
			a:      -14,
			b:      2,
			result: -28,
		},
	}
	calc := newCalc()

	for _, tt := range testCalc {
		calc.SetA(tt.a)
		calc.SetB(tt.b)
		calc.Do(multiply)
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, calc.Result(), tt.result)
		})
	}

}

func Test_divide(t *testing.T) {
	testCalc := [8]struct {
		name   string
		a      float64
		b      float64
		result float64
	}{
		{
			name:   "valid case 1",
			a:      2,
			b:      2,
			result: 1,
		},
		{
			name:   "valid case 2",
			a:      30,
			b:      -3,
			result: -10,
		},
		{
			name:   "valid case 3",
			a:      0,
			b:      2,
			result: 0,
		},
		{
			name:   "valid case 4",
			a:      10,
			b:      20,
			result: 0.5,
		},
		{
			name:   "valid case 5",
			a:      -5,
			b:      2,
			result: -2.5,
		},
		{
			name:   "valid case 6",
			a:      100,
			b:      1,
			result: 100,
		},
		{
			name:   "valid case 7",
			a:      -4,
			b:      -2,
			result: 2,
		},
		{
			name:   "invalid case 8",
			a:      30,
			b:      0,
			result: -0,
		},
	}
	calc := newCalc()

	for ind, tt := range testCalc {
		calc.SetA(tt.a)
		calc.SetB(tt.b)
		calc.Do(divide)
		if ind+1 == 8 { //  Не хотел много переписывать) не нашёл как тут панику обрабатывать
			t.Run(tt.name, func(t *testing.T) { //  Ведь ошибоку вроде нам нельзя добавлять по заданию
				assert.NotEqual(t, calc.Result(), tt.result)
			})
			continue
		}
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, calc.Result(), tt.result)
		})
	}
}

func Test_sum(t *testing.T) {
	testCalc := [8]struct {
		name   string
		a      float64
		b      float64
		result float64
	}{
		{
			name:   "valid case 1",
			a:      2,
			b:      2,
			result: 4,
		},
		{
			name:   "valid case 2",
			a:      2,
			b:      0,
			result: 2,
		},
		{
			name:   "valid case 3",
			a:      0,
			b:      2,
			result: 2,
		},
		{
			name:   "valid case 4",
			a:      0,
			b:      0,
			result: 0,
		},
		{
			name:   "valid case 5",
			a:      -5,
			b:      2.5,
			result: -2.5,
		},
		{
			name:   "valid case 6",
			a:      2,
			b:      -5,
			result: -3,
		},
		{
			name:   "valid case 7",
			a:      -2,
			b:      -2,
			result: -4,
		},
		{
			name:   "valid case 8",
			a:      123,
			b:      249,
			result: 372,
		},
	}
	calc := newCalc()

	for _, tt := range testCalc {
		calc.SetA(tt.a)
		calc.SetB(tt.b)
		calc.Do(sum)
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, calc.Result(), tt.result)
		})
	}

}

func Test_average(t *testing.T) {
	testCalc := [8]struct {
		name   string
		a      float64
		b      float64
		result float64
	}{
		{
			name:   "valid case 1",
			a:      2,
			b:      2,
			result: 2,
		},
		{
			name:   "valid case 2",
			a:      2,
			b:      0,
			result: 1,
		},
		{
			name:   "valid case 3",
			a:      0,
			b:      2,
			result: 1,
		},
		{
			name:   "valid case 4",
			a:      0,
			b:      0,
			result: 0,
		},
		{
			name:   "valid case 5",
			a:      -5,
			b:      2,
			result: -1.5,
		},
		{
			name:   "valid case 6",
			a:      100,
			b:      100,
			result: 100,
		},
		{
			name:   "valid case 7",
			a:      -2,
			b:      -2,
			result: -2,
		},
		{
			name:   "valid case 8",
			a:      -14,
			b:      2,
			result: -6,
		},
	}
	calc := newCalc()

	for _, tt := range testCalc {
		calc.SetA(tt.a)
		calc.SetB(tt.b)
		calc.Do(average)
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, calc.Result(), tt.result)
		})
	}

}
