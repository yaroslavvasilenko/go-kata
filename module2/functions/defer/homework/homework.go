package main

import (
	"errors"
	"fmt"
	"log"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

// BEGIN (write your solution here)
func main() {
	sliceDict := [][]map[string]string{
		{{"a": "b"}, nil},
		{{"a": "b"}, {"b": "c"}},
		{},
	}

	for _, value := range sliceDict {
		dictStr := MergeDictsJob{
			Dicts:  value,
			Merged: make(map[string]string),
		}
		if err := dictStr.merge(); err != nil {
			log.Println(err)
			continue
		}
		fmt.Println(dictStr)
	}

}

func (md *MergeDictsJob) merge() error {
	defer md.finish()
	if len(md.Dicts) <= 1 {
		return errNotEnoughDicts
	}

	for _, dict := range md.Dicts {
		if dict == nil {
			return errNilDict
		}

		for k, v := range dict {
			md.Merged[k] = v
		}
	}

	return nil
}

func (md *MergeDictsJob) finish() {
	md.IsFinished = true
}

// END

// Пример работы
// ExecuteMergeDictsJob(&MergeDictsJob{})
//&MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"

// ExecuteMergeDictsJob(&MergeDictsJob{Dicts:
//[]map[string]string{{"a": "b"},nil}})
//&MergeDictsJob{IsFinished: true,
//Dicts: []map[string]string{{"a": "b"},
//nil}}, "nil dictionary"

// ExecuteMergeDictsJob(&MergeDictsJob{Dicts:
//[]map[string]string{{"a": "b"},{"b": "c"}}})
//&MergeDictsJob{IsFinished: true,
//Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil
