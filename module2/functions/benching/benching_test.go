package benching_test

import "testing"

func BenchmarkInsertIntMap10000(b *testing.B) {
	insertXIntMap(10000, b)

}

func insertXIntMap(x int, b *testing.B) {
	// Инициализируем Map и вставляем X элементов
	testmap := make(map[int]int, 0)
	// Сбрасываем таймер после инициализации Map
	b.ResetTimer()
	for i := 0; i < x; i++ {
		// Вставляем значение I в ключ I.
		testmap[i] = i
	}
}
