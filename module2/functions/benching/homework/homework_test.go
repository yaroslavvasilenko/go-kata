package main

import (
	"testing"
)

func BenchmarkMapUserProducts(b *testing.B) {
	users := genUsers()
	products := genProducts()

	b.ResetTimer()
	_ = MapUserProducts(users, products)
}

func BenchmarkMapUserProducts2(b *testing.B) {
	users := genUsers()
	products := genProducts()

	b.ResetTimer()
	_ = MapUserProducts2(users, products)
}
