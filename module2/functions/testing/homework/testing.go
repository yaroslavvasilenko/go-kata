package greet

import (
	"fmt"
	"strings"
)

func Greet(name string) string {
	var result string
	if len(name) == len([]rune(name)) {
		result = fmt.Sprintf("Hello %s, you welcome!", name)

	} else {
		name = strings.ToLower(name) //  не знаю может ошибка в тесте, но имя в want с маленькой буквы, а аргументе с большой
		result = fmt.Sprintf("Привет %s, добро пожаловать!", name)
	}

	return result
}
