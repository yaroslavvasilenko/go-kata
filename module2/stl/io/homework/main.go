// You can edit this code!
// Click here and start typing.
package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
)

const filePath = "./module2/stl/io/homework/temp/example.txt"

func check(err error) {
	if err != nil {
		log.Panic(err)
	}
}

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	buf := bytes.NewBuffer(nil)

	// запишите данные в буфер
	for _, line := range data {
		buf.WriteString(line + "\n")
	}

	// создайте файл
	f, err := os.Create(filePath)
	defer check(err)
	defer func() {
		if cerr := f.Close(); cerr != nil && err == nil {
			err = cerr
		}
	}()
	defer check(err)

	// запишите данные в файл
	_, err = f.Write(buf.Bytes())
	check(err)

	// прочтите данные в новый буфер
	bufSecond := bytes.NewBuffer(nil)

	_, err = f.Seek(0, 0)
	check(err)

	_, err = bufSecond.ReadFrom(f)
	check(err)

	// выведите данные из буфера buf.String()
	fmt.Println(bufSecond.String())
	// у вас все получится!
}
