package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

type Config struct {
	AppName    string `json:"app_name"`
	Production bool   `json:"production"`
}

func main() {
	configPath := flag.String("config", "", "path config")
	flag.Parse()
	f, err := os.ReadFile(*configPath)
	if err != nil {
		log.Println(err)
		return
	}
	if !json.Valid(f) {
		log.Println("no valid")
	}

	conf := &Config{}
	err = json.Unmarshal(f, conf)
	if err != nil {
		log.Println(err)
		return
	}

	fmt.Printf("%+v\n", *conf)
}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение
