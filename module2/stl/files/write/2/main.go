package main

import (
	"bytes"
	"log"
	"os"
)

const filePath = "./module2/stl/files/tmp/"

func check(err error) {
	if err != nil {
		log.Panic()
	}
}

func main() {
	fileByte, err := os.ReadFile(filePath + "example.txt")
	check(err)

	for cyrillic, latin := range ruTransiltMap {
		tempCyrillicChar := []byte(cyrillic)
		tempLatinChar := []byte(latin)
		fileByte = bytes.Replace(fileByte, tempCyrillicChar, tempLatinChar, -1)
	}
	f, err := os.Create(filePath + "example.processed.txt")
	check(err)

	defer check(err)
	defer func() {
		if cerr := f.Close(); cerr != nil && err == nil {
			err = cerr
		}
	}()
	defer check(f.Sync())

	_, err = f.Write(fileByte)
	check(err)

}

var ruTransiltMap = map[string]string{
	"а": "a",
	"б": "b",
	"в": "v",
	"г": "g",
	"д": "d",
	"е": "e",
	"ё": "yo",
	"ж": "zh",
	"з": "z",
	"и": "i",
	"й": "j",
	"к": "k",
	"л": "l",
	"м": "m",
	"н": "n",
	"о": "o",
	"п": "p",
	"р": "r",
	"с": "s",
	"т": "t",
	"у": "u",
	"ф": "f",
	"х": "h",
	"ц": "c",
	"ч": "ch",
	"ш": "sh",
	"щ": "sch",
	"ъ": "'",
	"ы": "y",
	"ь": "",
	"э": "e",
	"ю": "ju",
	"я": "ja",
	"А": "A",
	"Б": "B",
	"В": "V",
	"Г": "G",
	"Д": "D",
	"Е": "E",
	"Ё": "Yo",
	"Ж": "Zh",
	"З": "Z",
	"И": "I",
	"Й": "J",
	"К": "K",
	"Л": "L",
	"М": "M",
	"Н": "N",
	"О": "O",
	"П": "P",
	"Р": "R",
	"С": "S",
	"Т": "T",
	"У": "U",
	"Ф": "F",
	"Х": "H",
	"Ц": "C",
	"Ч": "Ch",
	"Ш": "Sh",
	"Щ": "Sch",
	"Ы": "Y",
	"Э": "E",
	"Ю": "Ju",
	"Я": "Ja",
}
