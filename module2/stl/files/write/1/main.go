package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

const filePath = "./module2/stl/files/tmp/datWrite"

func check(err error) {
	if err != nil {
		log.Panic()
	}
}

func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your name: ")
	name, _ := reader.ReadString('\n')
	fmt.Printf("Hello %s\n", name)

	f, err := os.Create(filePath)
	if err != nil {
		log.Panic(err)
	}
	defer check(err)
	defer func() {
		if cerr := f.Close(); cerr != nil && err == nil {
			err = cerr
		}
	}()
	defer check(f.Sync())

	_, err = f.WriteString(name)
	check(err)

}
