package main

import "fmt"

func main() {
	generateSelfStory("Санёчик", 17, 1000000.1)
}
func generateSelfStory(name string, age int, money float64) {
	fmt.Printf("Привет все я %s, мне %d лет.\nCегодня я вам расскажу как заработать %7.2f$ за месяц работы со мной, даже если вам 13 лет",
		name, age, money)
}
