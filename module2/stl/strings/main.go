package main

import (
	"fmt"
	"strconv"
	"strings"
)

type Cache struct {
	data map[string]*User
}

type User struct {
	ID       int
	Nickname string
	Email    string
}

func main() {
	var users []*User
	emails := []string{"robpike@gmail.com", "davecheney@gmail.com", "bradfitzpatrick@email.ru", "eliben@gmail.com", "quasilyte@mail.ru"}

	for i, mail := range emails {
		users = append(users, &User{
			ID:       i + 1,
			Nickname: strings.Split(mail, "@")[0],
			Email:    mail,
		})
	}

	cache := NewCache()
	for _, user := range users {
		key := []string{user.Nickname, ":", strconv.Itoa(user.ID)}
		cache.Set(strings.Join(key, ""), user)
	}

	keys := []string{"robpike:1", "davecheney:2", "bradfitzpatrick:3", "eliben:4", "quasilyte:5"}
	for _, key := range keys {
		fmt.Println(*cache.Get(key))
	}
}

func NewCache() *Cache {
	return &Cache{data: make(map[string]*User, 100)}
}

func (c *Cache) Set(key string, u *User) {
	c.data[key] = u
}

func (c *Cache) Get(key string) *User {
	return c.data[key]
}
