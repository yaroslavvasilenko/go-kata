package main

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	database, _ := sql.Open("sqlite3", "./nraboy.db")
	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	_, err := statement.Exec()
	if err != nil {
		log.Println(err)
		return
	}
	statement, _ = database.Prepare("INSERT INTO people (firstname, lastname) VALUES (?, ?)")
	_, err = statement.Exec("Nic", "Raboy")
	if err != nil {
		log.Println(err)
		return
	}
	rows, _ := database.Query("SELECT id, firstname, lastname FROM people")
	var id int
	var firstname string
	var lastname string
	for rows.Next() {
		if err := rows.Scan(&id, &firstname, &lastname); err != nil {
			log.Println(err)
			return
		}
		fmt.Println(strconv.Itoa(id) + ": " + firstname + " " + lastname)
	}
}
