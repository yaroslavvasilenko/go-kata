package json_test

import (
	"testing"

	"gitlab.com/yaroslavvasilenko/go-kata/module2/optimization/json"
)

var jsonData = []byte(`[
  {
    "id": 0,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "doggie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "available"
  }
]`)

func BenchmarkUnmarshal(b *testing.B) {
	var (
		pets json.Pets
		err  error
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = json.Unmarshal(jsonData)
		if err != nil {
			panic(err)
		}
		_ = pets
	}
}

func BenchmarkMarshal(b *testing.B) {
	var (
		pets json.Pets
		err  error
		data []byte
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

func BenchmarkUnmarshalJ(b *testing.B) {
	var (
		pets json.Pets
		err  error
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = json.UnmarshalJsoniter(jsonData)
		if err != nil {
			panic(err)
		}
		_ = pets
	}
}

func BenchmarkMarshalJ(b *testing.B) {
	var (
		pets json.Pets
		err  error
		data []byte
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = pets.MarshalJsoniter()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}
