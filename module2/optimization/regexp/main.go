package regexp

import (
	"regexp"
)

var umlauts = "üÜöÖäÄèàùÈÀÙêâôîûÊÂÔÎÛëïÿËÏŸçÇéÉ"

var replaceWords = map[string]string{
	"(?i)\\bimac\\b":    "iMac",
	"(?i)\\biphone\\b":  "iPhone",
	"(?i)\\bipad\\b":    "iPad",
	"(?i)\\bipod\\b":    "iPod",
	"(?i)\\bmacbook\\b": "MacBook",
}

var replacements = []string{
	`(?i)(\\?\\<\\=\\^|\\s)[.*#=!]+([0-9A-ZА-ЯЁҐЄIЇ\x{0456}\x{0457}` + umlauts + `]+)[.*#=!]+(\\?\\=\\s|$)`,
	`(?i)[^()+\/\\\!;:, \."«»*0-9A-ZА-Я\\–\\—#№ЁҐЄIЇ` + umlauts + `\x{0456}\x{0457}²’‘“”\\'&-]`,
	`^(?i)[^«»'0-9A-ZА-Я\\–\\—ЁҐЄIЇ` + umlauts + "]",
	"^(?i)[^0-9A-ZА-ЯҐЄIЇ" + umlauts + "]$",
	"[<\\[{]",
	"[>\\]}]",
	"[_]",
	"^[() +\\/,.-]+",
	"[(\\/ ,.-]+$",
	"!+",
	umlauts,
}

var replacementsMap = map[string]string{
	`(?i)(\\?\\<\\=\\^|\\s)[.*#=!]+([0-9A-ZА-ЯЁҐЄIЇ\x{0456}\x{0457}` + umlauts + `]+)[.*#=!]+(\\?\\=\\s|$)`: "${1}",
	`(?i)[^()+\/\\\!;:, \."«»*0-9A-ZА-Я\\–\\—#№ЁҐЄIЇ` + umlauts + `\x{0456}\x{0457}²’‘“”\\'&-]`:             "",
	`^(?i)[^«»'0-9A-ZА-Я\\–\\—ЁҐЄIЇ` + umlauts + "]":                                                        "",
	"^(?i)[^0-9A-ZА-ЯҐЄIЇ" + umlauts + "]$":                                                                 "",
	"[<\\[{]":        "(",
	"[>\\]}]":        ")",
	"[_]":            " ",
	"^[() +\\/,.-]+": "",
	"[(\\/ ,.-]+$":   "",
	"!+":             "",
	umlauts:          "",
}

var (
	replacementMapRe = make(map[string]*regexp.Regexp, len(replacementsMap))
)

func init() {
	for _, pattern := range replacements {
		replacementMapRe[pattern] = regexp.MustCompile(pattern)
	}
}

type FilterText struct {
	replacements     []string
	replacementsMap  map[string]string
	replacementMapRe map[string]*regexp.Regexp
}

func NewFilterText() *FilterText {
	repMap := map[string]string{
		`(?i)(\\?\\<\\=\\^|\\s)[.*#=!]+([0-9A-ZА-ЯЁҐЄIЇ\x{0456}\x{0457}` + umlauts + `]+)[.*#=!]+(\\?\\=\\s|$)`: "${1}",
		`(?i)[^()+\/\\\!;:, \."«»*0-9A-ZА-Я\\–\\—#№ЁҐЄIЇ` + umlauts + `\x{0456}\x{0457}²’‘“”\\'&-]`:             "",
		`^(?i)[^«»'0-9A-ZА-Я\\–\\—ЁҐЄIЇ` + umlauts + "]":                                                        "",
		"^(?i)[^0-9A-ZА-ЯҐЄIЇ" + umlauts + "]$":                                                                 "",
		"[<\\[{]":           "(",
		"[>\\]}]":           ")",
		"[_]":               " ",
		"^[() +\\/,.-]+":    "",
		"[(\\/ ,.-]+$":      "",
		"!+":                "",
		umlauts:             "",
		"(?i)\\bimac\\b":    "iMac",
		"(?i)\\biphone\\b":  "iPhone",
		"(?i)\\bipad\\b":    "iPad",
		"(?i)\\bipod\\b":    "iPod",
		"(?i)\\bmacbook\\b": "MacBook",
	}
	filterText := &FilterText{
		replacements: []string{
			`(?i)(\\?\\<\\=\\^|\\s)[.*#=!]+([0-9A-ZА-ЯЁҐЄIЇ\x{0456}\x{0457}` + umlauts + `]+)[.*#=!]+(\\?\\=\\s|$)`,
			`(?i)[^()+\/\\\!;:, \."«»*0-9A-ZА-Я\\–\\—#№ЁҐЄIЇ` + umlauts + `\x{0456}\x{0457}²’‘“”\\'&-]`,
			`^(?i)[^«»'0-9A-ZА-Я\\–\\—ЁҐЄIЇ` + umlauts + "]",
			"^(?i)[^0-9A-ZА-ЯҐЄIЇ" + umlauts + "]$",
			"[<\\[{]",
			"[>\\]}]",
			"[_]",
			"^[() +\\/,.-]+",
			"[(\\/ ,.-]+$",
			"!+",
			umlauts,
			"(?i)\\bimac\\b",
			"(?i)\\biphone\\b",
			"(?i)\\bipad\\b",
			"(?i)\\bipod\\b",
			"(?i)\\bmacbook\\b",
		},
		replacementsMap:  repMap,
		replacementMapRe: make(map[string]*regexp.Regexp, len(repMap)),
	}

	for _, pattern := range filterText.replacements {
		filterText.replacementMapRe[pattern] = regexp.MustCompile(pattern)
	}

	return filterText
}

func SanitizeText(text string) string {
	for _, pattern := range replacements {
		re := regexp.MustCompile(pattern)
		text = re.ReplaceAllString(text, replacementsMap[pattern])
	}

	for pattern, replacement := range replaceWords {
		re := regexp.MustCompile(pattern)
		text = re.ReplaceAllString(text, replacement)
	}
	return text
}

func (ft *FilterText) SanitizeText2(text string) string {
	for _, pattern := range replacements {
		text = ft.replacementMapRe[pattern].ReplaceAllString(text, replacementsMap[pattern])
	}

	for pattern, replacement := range replaceWords {
		re := regexp.MustCompile(pattern)
		text = re.ReplaceAllString(text, replacement)
	}
	return text
}

func (ft *FilterText) SanitizeText3(text string) string {
	for _, pattern := range replacements {
		text = ft.replacementMapRe[pattern].ReplaceAllString(text, replacementsMap[pattern])
	}

	for pattern, rep := range replaceWords {
		text = ft.replacementMapRe[pattern].ReplaceAllString(text, rep)
	}
	return text
}
