package regexp_test

import (
	"testing"

	"gitlab.com/yaroslavvasilenko/go-kata/module2/optimization/regexp"
)

func BenchmarkFilterText_SanitizeText(b *testing.B) {
	for i := 0; i < len(data); i++ {
		regexp.SanitizeText(data[i])
	}
}

func BenchmarkFilterText_SanitizeText2(b *testing.B) {
	ft := regexp.NewFilterText()
	b.ResetTimer()
	for i := 0; i < len(data); i++ {
		ft.SanitizeText2(data[i])
	}
}

func BenchmarkFilterText_SanitizeText3(b *testing.B) {
	ft := regexp.NewFilterText()
	b.ResetTimer()
	for i := 0; i < len(data); i++ {
		ft.SanitizeText3(data[i])
	}

}

var data = []string{
	`üÜöÖäÄèàùÈÀÙêâôîûÊÂÔÎÛëïÿËÏŸçÇéÉ Lorem ipsum ipod sit amet, consectetur adipiscing elit. Aliquam lobortis,!  (ante) vitae rutrum mollis, augue ligula luctus nunc, vel blandit massa lacus eu magna. Nulla sit amet molestie velit, ac auctor velit. Morbi erat quam, varius id augue sed, molestie vestibulum urna. Pellentesque sit amet ---- ligula libero. Nam scelerisque urna sit amet laoreet scelerisque. Duis id sapien enim. Praesent fermentum pharetra mauris id tincidunt. Fusce porta augue ut sodales euismod. Praesent eget ligula imac, hendrerit nulla et, interdum turpis. Etiam rhoncus efficitur libero at tempus. Integer blandit mi et ornare rhoncus. Nulla in odio et dui sagittis hendrerit eu iphone lorem. Suspendisse consectetur pharetra tellus, fringilla pulvinar ex egestas vel.`,

	`üÜöÖäÄèàùÈÀÙêâôîûÊÂÔÎÛëïÿËÏŸçÇéÉ Morbi volutpat risus porta orci finibus porttitor. Integer ipsum nisl, feugiat eu velit id, venenatis fermentum ex. Suspendisse potenti. Maecenas viverra viverra posuere. Nulla vitae pulvinar est. In magna eros, sagittis in ex cursus, consequat tempus quam. Mauris molestie, ex vitae aliquam facilisis,----- purus diam pharetra felis, ut tristique augue velit in purus. Fusce laoreet, sapien eget imperdiet sollicitudin, dui magna sagittis lorem, sed vulputate lectus elit nec justo. Vestibulum elit risus, fringilla non nunc id, ornare blandit mi. Pellentesque ac tincidunt felis. Mauris aliquet scelerisque orci, ac molestie tortor imperdiet eget. Integer rhoncus lectus in augue feugiat sollicitudin. Nunc porta ut purus et molestie.`,

	`üÜöÖäÄèàùÈÀÙêâôîûÊÂÔÎÛëïÿËÏŸçÇéÉ Lorem ipsum ipod sit amet, consectetur adipiscing elit. Pellentesque eu libero vel tellus vestibulum ipod iphone et lectus. Praesent sollicitudin placerat risus iphone dapibus. Proin non ipod eget enim tempus malesuada. iphoneque id diam eu eros viverra pellentesque. Donec iphone ante dapibus (lectus) faucibus blandit. Suspendisse euismod blandit magna, non facilisis arcu tincidunt eu. Donec pharetra in nisl non efficitur.`,

	`üÜöÖäÄèàùÈÀÙêâôîûÊÂÔÎÛëïÿËÏŸçÇéÉ Duis massa orci, pellentesque at ----- nulla at, venenatis - consequat eros. Donec sit amet vehicula urna. Vivamus imac leo non aliquam faucibus. Vestibulum vel varius erat. Fusce dapibus posuere orci, vitae vulputate mauris tristique aliquam. In vel sapien non felis rutrum tincidunt. Nullam auctor purus sed nulla sollicitudin euismod.`,

	`üÜöÖäÄèàùÈÀÙêâôîûÊÂÔÎÛëïÿËÏŸçÇéÉ Donec semper dui iphone ex sollicitudin, non imac felis {dignissim}. Integer macbook aliquam magna, ipod commodo orci posuere a. iphoneque accumsan felis sit amet lacinia vehicula. Morbi velit libero, bibendum non sapien id, molestie maximus arcu. Maecenas aliquam, justo eget mollis porta, nisi ipod sagittis justo, in vehicula augue elit iphone purus. Maecenas magna elit, pulvinar vel luctus fermentum, consequat suscipit est. Morbi laoreet sem eu massa convallis pharetra. Ut iaculis dui eget elit hendrerit venenatis. Sed sed lacus ut turpis dignissim ipod iphone sed tortor. Nulla rhoncus turpis a nulla congue, eget posuere metus porta. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec vel est sem. Nulla vitae tortor sit amet ipsum sodales ornare eu a nunc.`,
}
