package repo

import (
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/model"
	"sync"
)

type OrderStorage struct {
	data               []*model.Order
	primaryKeyIDx      map[int]*model.Order
	autoIncrementCount int
	sync.RWMutex
}

func NewOrderStorage() *OrderStorage {
	return &OrderStorage{
		data:               make([]*model.Order, 0, 13),
		primaryKeyIDx:      make(map[int]*model.Order, 13),
		autoIncrementCount: 1,
	}
}

func (o *OrderStorage) Create(order model.Order) model.Order {
	o.Lock()
	defer o.Unlock()
	order.ID = o.autoIncrementCount
	o.primaryKeyIDx[order.ID] = &order
	o.autoIncrementCount++
	o.data = append(o.data, &order)

	return order
}

func (o *OrderStorage) Delete(orderID int) error {
	o.Lock()
	defer o.Unlock()
	if _, ok := o.primaryKeyIDx[orderID]; !ok {
		return notFoundOrder
	}

	for i := 0; i < len(o.data); i++ {
		if o.data[i].ID == orderID {
			o.data = append(o.data[:i], o.data[i+1:]...)
			delete(o.primaryKeyIDx, orderID)
		}
	}

	return nil
}

func (o *OrderStorage) GetByID(petID int) (model.Order, error) {
	o.RLock()
	defer o.RUnlock()
	if elem, ok := o.primaryKeyIDx[petID]; ok {
		return *elem, nil
	}

	return model.Order{}, notFoundOrder
}

func (o *OrderStorage) GetListByStatus() map[string]int {
	res := map[string]int{"available": 0, "pending": 0, "sold": 0}

	o.RLock()
	defer o.RUnlock()
	for _, elem := range o.data {
		res[elem.Status] += 1
	}
	return res
}
