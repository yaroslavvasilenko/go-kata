package repo

import (
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/model"
	"reflect"
	"sync"
	"testing"
)

var standartCasePet = []*model.Pet{
	{ID: 1,
		Category: model.Category{
			ID:   0,
			Name: "test category",
		},
		Name: "Alma",
		PhotoUrls: []string{
			"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
			"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
			"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
		},
		Tags:   []model.Category{},
		Status: "available",
	},
	{ID: 2,
		Category: model.Category{
			ID:   0,
			Name: "test category2",
		},
		Name: "Alma2",
		PhotoUrls: []string{
			"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
			"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
			"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
		},
		Tags:   []model.Category{},
		Status: "pending",
	},
	{ID: 3,
		Category: model.Category{
			ID:   0,
			Name: "test category3",
		},
		Name: "Alma3",
		PhotoUrls: []string{
			"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
			"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
			"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
		},
		Tags:   []model.Category{},
		Status: "sold",
	},
}

var standartPrimaryKeyIDxPet = map[int]*model.Pet{1: standartCasePet[0], 2: standartCasePet[1], 3: standartCasePet[2]}
var standartIncrementPet = 3

func TestPetStorage_Create(t *testing.T) {
	type fields struct {
		data               []*model.Pet
		primaryKeyIDx      map[int64]*model.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
	}
	type args struct {
		pet model.Pet
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   model.Pet
	}{
		{
			name: "first test",
			args: args{
				pet: model.Pet{
					ID: 0,
					Category: model.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []model.Category{},
					Status: "active",
				},
			},
			want: model.Pet{
				ID: 0,
				Category: model.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []model.Category{},
				Status: "active",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewPetStorage()
			var got model.Pet
			var err error
			got = p.Create(tt.args.pet)
			tt.want.ID = got.ID // fix autoincrement value
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if got, err = p.GetByID(got.ID); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}

func TestPetStorage_Delete(t *testing.T) {
	type fields struct {
		data               []*model.Pet
		primaryKeyIDx      map[int]*model.Pet
		autoIncrementCount int
		RWMutex            sync.RWMutex
	}
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{data: standartCasePet,
				primaryKeyIDx:      standartPrimaryKeyIDxPet,
				autoIncrementCount: len(standartCasePet),
			},
			args:    args{petID: standartCasePet[1].ID},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				RWMutex:            tt.fields.RWMutex,
			}
			if err := p.Delete(tt.args.petID); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPetStorage_GetByID(t *testing.T) {
	type fields struct {
		data               []*model.Pet
		primaryKeyIDx      map[int]*model.Pet
		autoIncrementCount int
		RWMutex            sync.RWMutex
	}
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Pet
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{data: standartCasePet,
				primaryKeyIDx:      standartPrimaryKeyIDxPet,
				autoIncrementCount: len(standartCasePet),
			},
			args:    args{petID: standartCasePet[1].ID},
			want:    *standartCasePet[1],
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				RWMutex:            tt.fields.RWMutex,
			}
			got, err := p.GetByID(tt.args.petID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_GetListByStatus(t *testing.T) {
	type fields struct {
		data               []*model.Pet
		primaryKeyIDx      map[int]*model.Pet
		autoIncrementCount int
		RWMutex            sync.RWMutex
	}
	type args struct {
		status string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []model.Pet
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{data: append(standartCasePet, standartCasePet[0]),
				primaryKeyIDx:      standartPrimaryKeyIDxPet,
				autoIncrementCount: len(standartCasePet),
			},
			args:    args{standartCasePet[0].Status},
			want:    []model.Pet{*standartCasePet[0], *standartCasePet[0]},
			wantErr: false,
		},
		{
			name: "second test - 2",
			fields: fields{data: append(standartCasePet, standartCasePet[0]),
				primaryKeyIDx:      standartPrimaryKeyIDxPet,
				autoIncrementCount: len(standartCasePet),
			},
			args:    args{"error"},
			want:    nil,
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				RWMutex:            tt.fields.RWMutex,
			}
			got, err := p.GetListByStatus(tt.args.status)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetListByStatus() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetListByStatus() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_Update(t *testing.T) {
	type fields struct {
		data               []*model.Pet
		primaryKeyIDx      map[int]*model.Pet
		autoIncrementCount int
		RWMutex            sync.RWMutex
	}
	type args struct {
		pet model.Pet
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Pet
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{data: standartCasePet,
				primaryKeyIDx:      standartPrimaryKeyIDxPet,
				autoIncrementCount: len(standartCasePet),
			},
			args: args{func() model.Pet {
				r := *standartCasePet[1]
				r.Name = "Ursula"
				return r
			}()},
			want: func() model.Pet {
				r := *standartCasePet[1]
				r.Name = "Ursula"
				return r
			}(),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PetStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				RWMutex:            tt.fields.RWMutex,
			}
			_, err := p.Update(tt.args.pet)
			got, _ := p.GetByID(tt.args.pet.ID)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
		})
	}
}
