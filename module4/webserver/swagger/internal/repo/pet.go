package repo

import (
	"errors"
	"fmt"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/model"
	"sync"
)

var AvailableStatus = map[string]bool{"available": true, "pending": true, "sold": true}

var notFoundOrder = fmt.Errorf("order not found")

type PetStorage struct {
	data               []*model.Pet
	primaryKeyIDx      map[int]*model.Pet
	autoIncrementCount int
	sync.RWMutex
}

func NewPetStorage() *PetStorage {
	return &PetStorage{
		data:               make([]*model.Pet, 0, 13),
		primaryKeyIDx:      make(map[int]*model.Pet, 13),
		autoIncrementCount: 1,
	}
}

func (p *PetStorage) Create(pet model.Pet) model.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)

	return pet
}

func (p *PetStorage) Update(pet model.Pet) (model.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.primaryKeyIDx[pet.ID]; ok {
		for i := 0; i < len(p.data); i++ {
			if p.data[i].ID == pet.ID {
				p.data[i] = &pet
				p.primaryKeyIDx[pet.ID] = &pet
			}
		}
	} else {
		return model.Pet{}, notFoundOrder
	}

	return model.Pet{}, nil
}

func (p *PetStorage) Delete(petID int) error {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.primaryKeyIDx[petID]; !ok {
		return notFoundOrder
	}

	for i := 0; i < len(p.data); i++ {
		if p.data[i].ID == petID {
			p.data = append(p.data[:i], p.data[i+1:]...)
			delete(p.primaryKeyIDx, petID)
		}
	}

	return nil

}

func (p *PetStorage) GetByID(petID int) (model.Pet, error) {
	p.RLock()
	defer p.RUnlock()
	if elem, ok := p.primaryKeyIDx[petID]; ok {
		return *elem, nil
	}

	return model.Pet{}, notFoundOrder

}

func (p *PetStorage) GetListByStatus(status string) ([]model.Pet, error) {
	if _, ok := AvailableStatus[status]; !ok {
		return nil, errors.New("invalid status value")
	}

	p.RLock()
	defer p.RUnlock()

	var arrPet []model.Pet
	for _, elem := range p.data {
		if elem.Status == status {
			arrPet = append(arrPet, *elem)
		}

	}
	return arrPet, nil
}
