package repo

import (
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/model"
	"reflect"
	"sync"
	"testing"
)

var standartCaseOrder = []*model.Order{
	{
		ID:       1,
		OrderID:  1,
		Quantity: 1,
		ShipDate: "22.05.2022",
		Status:   "available",
		Complete: false,
	},
	{
		ID:       2,
		OrderID:  2,
		Quantity: 2,
		ShipDate: "23.05.2022",
		Status:   "pending",
		Complete: false,
	},
	{
		ID:       3,
		OrderID:  3,
		Quantity: 3,
		ShipDate: "24.05.2022",
		Status:   "sold",
		Complete: false,
	},
}

var standartPrimaryKeyIDxOrder = map[int]*model.Order{1: standartCaseOrder[0], 2: standartCaseOrder[1], 3: standartCaseOrder[2]}
var standartIncrementOrder = 3

func TestOrderStorage_Create(t *testing.T) {
	type fields struct {
		data               []*model.Order
		primaryKeyIDx      map[int]*model.Order
		autoIncrementCount int
		RWMutex            sync.RWMutex
	}
	type args struct {
		order model.Order
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   model.Order
	}{
		{
			name: "first case",
			args: args{*standartCaseOrder[0]},
			want: *standartCaseOrder[0],
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := NewOrderStorage()
			got := o.Create(tt.args.order)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}

			var good = false
			for _, elem := range o.data {
				if *elem == got {
					good = true
				}
			}
			if !good {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			good = false
			if _, ok := o.primaryKeyIDx[got.ID]; ok {
				good = true
			}
			if !good {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}

		})
	}
}

func TestOrderStorage_Delete(t *testing.T) {
	type fields struct {
		data               []*model.Order
		primaryKeyIDx      map[int]*model.Order
		autoIncrementCount int
		RWMutex            sync.RWMutex
	}
	type args struct {
		orderID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    fields
		wantErr bool
	}{
		{
			name: "first case",
			fields: fields{
				data:               standartCaseOrder,
				primaryKeyIDx:      standartPrimaryKeyIDxOrder,
				autoIncrementCount: standartIncrementOrder,
			},
			args: args{standartCaseOrder[1].OrderID},
			want: fields{
				data: []*model.Order{standartCaseOrder[0], standartCaseOrder[2]},
				primaryKeyIDx: func() map[int]*model.Order {
					r := standartPrimaryKeyIDxOrder
					delete(r, 1)
					return r
				}(),
				autoIncrementCount: standartIncrementOrder,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				RWMutex:            tt.fields.RWMutex,
			}
			if err := o.Delete(tt.args.orderID); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(o, tt.want) {
				t.Errorf("Delete() error = %v, wantErr %v", tt.args, tt.want)
			}

		})
	}
}

func TestOrderStorage_GetByID(t *testing.T) {
	type fields struct {
		data               []*model.Order
		primaryKeyIDx      map[int]*model.Order
		autoIncrementCount int
		RWMutex            sync.RWMutex
	}
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Order
		wantErr bool
	}{
		{
			name: "first case",
			fields: fields{
				data:               standartCaseOrder,
				primaryKeyIDx:      standartPrimaryKeyIDxOrder,
				autoIncrementCount: standartIncrementOrder,
			},
			args:    args{standartCaseOrder[1].OrderID},
			want:    *standartCaseOrder[1],
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				RWMutex:            tt.fields.RWMutex,
			}
			got, err := o.GetByID(tt.args.petID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderStorage_GetListByStatus(t *testing.T) {
	type fields struct {
		data               []*model.Order
		primaryKeyIDx      map[int]*model.Order
		autoIncrementCount int
		RWMutex            sync.RWMutex
	}
	tests := []struct {
		name   string
		fields fields
		want   map[string]int
	}{
		{
			name: "first test",
			fields: fields{
				data:               append(standartCaseOrder, standartCaseOrder[0]),
				primaryKeyIDx:      standartPrimaryKeyIDxOrder,
				autoIncrementCount: standartIncrementOrder},
			want: map[string]int{"available": 2, "pending": 1, "sold": 1},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &OrderStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				RWMutex:            tt.fields.RWMutex,
			}
			if got := o.GetListByStatus(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetListByStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}
