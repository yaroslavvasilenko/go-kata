package repo

import (
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/model"
	"reflect"
	"sync"
	"testing"
)

var standartCaseUser = []*model.User{
	{
		ID:         1,
		Username:   "Ivan",
		FirstName:  "1",
		LastName:   "2",
		Email:      "yyyy@mail.ru",
		Password:   "ewq312534",
		Phone:      "7918822",
		UserStatus: 1,
	},
	{
		ID:         2,
		Username:   "Ivan1",
		FirstName:  "11",
		LastName:   "22",
		Email:      "uuuu@mail.ru",
		Password:   "1ewq312534",
		Phone:      "79188221",
		UserStatus: 1,
	},
	{
		ID:         3,
		Username:   "Ivan3",
		FirstName:  "3",
		LastName:   "4",
		Email:      "qqqq@mail.ru",
		Password:   "ewq31rr2534",
		Phone:      "791882542",
		UserStatus: 1,
	},
}

var standartPrimaryKeyIDxUser = map[int]*model.User{1: standartCaseUser[0], 2: standartCaseUser[1], 3: standartCaseUser[2]}
var standartIncrementUser = 3

func TestUserStorage_Create(t *testing.T) {
	type fields struct {
		user model.User
	}
	type args struct {
		user model.User
	}
	tests := []struct {
		name string
		args args
		want model.User
	}{
		{
			name: "first case",
			args: args{*standartCaseUser[0]},
			want: *standartCaseUser[0],
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := NewUserStorage()
			u.Create(tt.args.user)
			got, err := u.GetByName(tt.args.user.Username)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if err != nil {
				t.Errorf("Create() = %v", err)
			}
		})
	}
}

func TestUserStorage_Delete(t *testing.T) {
	type fields struct {
		data               []*model.User
		primaryKeyIDx      map[int]*model.User
		autoIncrementCount int
		RWMutex            sync.RWMutex
	}
	type args struct {
		userID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *UserStorage
		wantErr bool
	}{
		{
			name: "first case",
			fields: fields{
				data:               standartCaseUser,
				primaryKeyIDx:      standartPrimaryKeyIDxUser,
				autoIncrementCount: standartIncrementUser,
			},
			args: args{standartCaseUser[1].ID},
			want: &UserStorage{
				data:               []*model.User{standartCaseUser[0], standartCaseUser[2]},
				primaryKeyIDx:      map[int]*model.User{1: standartCaseUser[0], 3: standartCaseUser[2]},
				autoIncrementCount: 3,
				RWMutex:            sync.RWMutex{},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				RWMutex:            tt.fields.RWMutex,
			}

			err := u.Delete(tt.args.userID)
			if !reflect.DeepEqual(u, tt.want) {
				t.Errorf("Delete() = %v, want %v", u, tt.want)
			}

			if (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserStorage_GetByName(t *testing.T) {
	type fields struct {
		data               []*model.User
		primaryKeyIDx      map[int]*model.User
		autoIncrementCount int
		RWMutex            sync.RWMutex
	}
	type args struct {
		username string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.User
		wantErr bool
	}{
		{
			name: "first case",
			fields: fields{
				data:               standartCaseUser,
				primaryKeyIDx:      standartPrimaryKeyIDxUser,
				autoIncrementCount: standartIncrementUser,
			},
			args:    args{username: standartCaseUser[0].Username},
			want:    *standartCaseUser[0],
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				RWMutex:            tt.fields.RWMutex,
			}
			got, err := u.GetByName(tt.args.username)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByName() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_Update(t *testing.T) {
	type fields struct {
		data               []*model.User
		primaryKeyIDx      map[int]*model.User
		autoIncrementCount int
		RWMutex            sync.RWMutex
	}
	type args struct {
		user model.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.User
		wantErr bool
	}{
		{
			name: "first case",
			fields: fields{
				data:               standartCaseUser,
				primaryKeyIDx:      standartPrimaryKeyIDxUser,
				autoIncrementCount: standartIncrementUser,
			},
			args: args{user: func() model.User {
				r := standartCaseUser[1]
				r.Email = "uioasdoo@yandex.com"
				return *r
			}()},
			want: func() model.User {
				r := standartCaseUser[1]
				r.Email = "uioasdoo@yandex.com"
				return *r
			}(),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &UserStorage{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
				RWMutex:            tt.fields.RWMutex,
			}
			_, err := u.Update(tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			got, _ := u.GetByName(tt.args.user.Username)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
		})
	}
}
