package repo

import (
	"fmt"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/model"
	"sync"
)

var notFoundUser = fmt.Errorf("user not found")

var name = model.User{}

type UserStorage struct {
	data               []*model.User
	primaryKeyIDx      map[int]*model.User
	autoIncrementCount int
	sync.RWMutex
}

func NewUserStorage() *UserStorage {
	return &UserStorage{
		data:               make([]*model.User, 0, 13),
		primaryKeyIDx:      make(map[int]*model.User, 13),
		autoIncrementCount: 1,
	}
}

func (u *UserStorage) Create(user model.User) model.User {
	u.Lock()
	defer u.Unlock()
	user.ID = u.autoIncrementCount
	u.primaryKeyIDx[user.ID] = &user
	u.autoIncrementCount++
	u.data = append(u.data, &user)

	return user
}

func (u *UserStorage) Update(user model.User) (model.User, error) {
	u.Lock()
	defer u.Unlock()
	if _, ok := u.primaryKeyIDx[user.ID]; ok {
		for i := 0; i < len(u.data); i++ {
			if u.data[i].ID == user.ID {
				u.data[i] = &user
			}
		}
	} else {
		return model.User{}, notFoundUser
	}

	return model.User{}, nil
}

func (u *UserStorage) Delete(userID int) error {
	u.Lock()
	defer u.Unlock()
	if _, ok := u.primaryKeyIDx[userID]; !ok {
		return notFoundUser
	}

	for i := 0; i < len(u.data); i++ {
		if u.data[i].ID == userID {
			u.data = append(u.data[:i], u.data[i+1:]...)
			delete(u.primaryKeyIDx, userID)
		}
	}

	return nil
}

func (u *UserStorage) GetByName(username string) (model.User, error) {
	u.RLock()
	defer u.RUnlock()
	for _, user := range u.data {
		if user.Username == username {
			return *user, nil
		}
	}

	return model.User{}, notFoundUser
}
