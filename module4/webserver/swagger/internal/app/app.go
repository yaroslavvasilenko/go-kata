package app

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/controller"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func Run(port string) {
	r := newRoute()

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}

func newRoute() *chi.Mux {
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	//  Pet
	petController := controller.NewPetController()
	r.Route("/pet", func(r chi.Router) {
		r.Post("/{petID}/uploadImage", petController.PetCreate)
		r.Post("/", petController.PetCreate)
		r.Put("/", petController.PetUpdate)
		r.Get("/findByStatus", petController.FindByStatus)
		r.Get("/{petID}", petController.PetGetByID)
		r.Post("/{petID}", petController.PetUpdatePost)
		r.Delete("/{petID}", petController.PetCreate)
	})

	//  Store
	orderController := controller.NewOrderController()
	r.Route("/store", func(r chi.Router) {
		r.Post("/order", orderController.OrderPlaced)
		r.Get("/order/{orderID}", orderController.OrderGetByID)
		r.Delete("/order/{orderID}", orderController.OrderDelete)
		r.Get("/inventory", orderController.GetInventory)
	})

	//  User
	userController := controller.NewUserController()
	r.Route("/user", func(r chi.Router) {
		r.Post("/createWithArray", userController.UsersCreate)
		r.Post("/createWithList", userController.UsersCreate)
		r.Get("/{username}", userController.UsernameGetByUsername)
		r.Put("/{username}", userController.UserUpdate)
		r.Delete("/{username}", userController.UserDelete)
		r.Post("/{username}", userController.UserCreate)
	})

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./module4/webserver/swagger/public"))).ServeHTTP(w, r)
	})

	return r
}
