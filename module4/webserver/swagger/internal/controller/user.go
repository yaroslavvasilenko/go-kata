package controller

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/model"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/repo"
	"net/http"
	"strconv"
)

type UserStorager interface {
	Create(user model.User) model.User
	Update(user model.User) (model.User, error)
	Delete(userID int) error
	GetByName(username string) (model.User, error)
}

type UserController struct { // User контроллер
	storage UserStorager
}

func NewUserController() *UserController { // конструктор нашего контроллера
	return &UserController{storage: repo.NewUserStorage()}
}

func (u *UserController) UserCreate(w http.ResponseWriter, r *http.Request) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //  400
	}

	user = u.storage.Create(user) // создаем запись в нашем storage
}

func (p *UserController) UsernameGetByUsername(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		user     model.User
		err      error
		username string
	)

	username = chi.URLParam(r, "username") // получаем petID из chi router
	if username == "" {
		if err != nil { // в случае ошибки отправляем Not Found код 400
			http.Error(w, err.Error(), http.StatusBadRequest)
			return // не забываем прекратить обработку нашего handler (ручки)
		}
	}

	user, err = p.storage.GetByName(username) // пытаемся получить Pet по id
	if err != nil {                           // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *UserController) UserUpdate(w http.ResponseWriter, r *http.Request) {
	var user model.User
	err := json.NewDecoder(r.Body).Decode(&user) // считываем приходящий json из *http.Request в структуру user

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	user, err = p.storage.Update(user)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(user)                            // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

}

func (p *UserController) UserDelete(w http.ResponseWriter, r *http.Request) {
	petIDRaw := chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err := strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                      // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = p.storage.Delete(petID) // пытаемся получить Pet по id
	if err != nil {               // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (u *UserController) UsersCreate(w http.ResponseWriter, r *http.Request) {
	var user []model.User
	err := json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest) //  400
	}
	for _, elem := range user {
		_ = u.storage.Create(elem) // создаем запись в нашем storage
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(user) // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}
