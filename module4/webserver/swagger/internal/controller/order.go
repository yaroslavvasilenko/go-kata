package controller

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/model"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/repo"
	"net/http"
	"strconv"
)

type OrderStorager interface {
	Create(pet model.Order) model.Order
	Delete(petID int) error
	GetByID(petID int) (model.Order, error)
	GetListByStatus() map[string]int
}

type OrderController struct { // Pet контроллер
	storage OrderStorager
}

func NewOrderController() *OrderController { // конструктор нашего контроллера
	return &OrderController{storage: repo.NewOrderStorage()}
}

func (o *OrderController) OrderPlaced(w http.ResponseWriter, r *http.Request) {
	var order model.Order
	err := json.NewDecoder(r.Body).Decode(&order) // считываем приходящий json из *http.Request в структуру Storage

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	order = o.storage.Create(order) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(order) // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (o *OrderController) OrderGetByID(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		order      model.Order
		err        error
		orderIDRaw string
		orderID    int
	)

	orderIDRaw = chi.URLParam(r, "orderID") // получаем orderID из chi router

	orderID, err = strconv.Atoi(orderIDRaw) // конвертируем в int
	if err != nil {                         // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	order, err = o.storage.GetByID(orderID) // пытаемся получить Pet по id
	if err != nil {                         // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(order)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (o *OrderController) OrderDelete(w http.ResponseWriter, r *http.Request) {
	orderIDRaw := chi.URLParam(r, "orderID") // получаем orderID из chi router

	orderID, err := strconv.Atoi(orderIDRaw) // конвертируем в int
	if err != nil {                          // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = o.storage.Delete(orderID) // пытаемся получить Order по id
	if err != nil {                 // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (o *OrderController) GetInventory(w http.ResponseWriter, r *http.Request) {
	mapStatus := o.storage.GetListByStatus()

	rawMapStatus, err := json.Marshal(mapStatus)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.WriteHeader(http.StatusOK)
	_, err = w.Write(rawMapStatus)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}
