package controller

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/model"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/repo"
	"io"
	"net/http"
	"os"
	"strconv"
)

const ImageStore = "./update/"

type PetStorager interface {
	Create(pet model.Pet) model.Pet
	Update(pet model.Pet) (model.Pet, error)
	Delete(petID int) error
	GetByID(petID int) (model.Pet, error)
	GetListByStatus(status string) ([]model.Pet, error)
}

type PetController struct { // Pet контроллер
	storage PetStorager
}

func NewPetController() *PetController { // конструктор нашего контроллера
	return &PetController{storage: repo.NewPetStorage()}
}

func (p *PetController) PetUploadImage(w http.ResponseWriter, r *http.Request) {
	petIDRaw := chi.URLParam(r, "petID")

	petID, err := strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                      // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err := p.storage.GetByID(petID) // пытаемся получить Pet по id
	if err != nil {                      // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	imageSave, fileHeader, err := r.FormFile("file")
	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	f, err := os.Create(ImageStore + fileHeader.Filename)
	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	_, err = io.Copy(f, imageSave)
	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet.PhotoUrls = append(pet.PhotoUrls, fileHeader.Filename)

	_, err = p.storage.Update(pet)
	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	w.WriteHeader(http.StatusOK)
}

func (p *PetController) PetCreate(w http.ResponseWriter, r *http.Request) {
	var pet model.Pet
	err := json.NewDecoder(r.Body).Decode(&pet) // считываем приходящий json из *http.Request в структуру Pet

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet = p.storage.Create(pet) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) PetUpdate(w http.ResponseWriter, r *http.Request) {
	var pet model.Pet
	err := json.NewDecoder(r.Body).Decode(&pet) // считываем приходящий json из *http.Request в структуру Pet

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err = p.storage.Update(pet)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

}

func (p *PetController) FindByStatus(w http.ResponseWriter, r *http.Request) {
	fileName := r.FormValue("status")

	pets, err := p.storage.GetListByStatus(fileName)
	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(pets)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

}

func (p *PetController) PetGetByID(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		pet      model.Pet
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err = p.storage.GetByID(petID) // пытаемся получить Pet по id
	if err != nil {                     // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(pet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *PetController) PetUpdatePost(w http.ResponseWriter, r *http.Request) {
	var (
		pet      model.Pet
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err = p.storage.GetByID(petID) // пытаемся получить Pet по id
	if err != nil {                     // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	name := r.FormValue("name")
	if name != "" {
		pet.Name = name
	}

	status := r.FormValue("status")
	if status != "" {
		pet.Status = status
	}

	_, err = p.storage.Update(pet)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат Pet json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

}

func (p *PetController) PetDelete(w http.ResponseWriter, r *http.Request) {
	petIDRaw := chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err := strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                      // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = p.storage.Delete(petID) // пытаемся получить Pet по id
	if err != nil {               // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}
