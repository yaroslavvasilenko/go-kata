package main

import (
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/app"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/swagger/internal/controller"
	"log"
	"os"
)

func init() {
	err := CreateStorageFile(controller.ImageStore)
	if err != nil {
		log.Println(err)
	}
}

func main() {
	app.Run(":8080")
}

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func CreateStorageFile(path string) error {
	ok, err := exists(path)
	if err != nil {
		return err
	}
	if !ok {
		err = os.Mkdir(path, os.ModePerm)
	}
	return err
}
