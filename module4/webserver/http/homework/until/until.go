package until

import (
	"os"
)

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func CreateStorageFile(path string) error {
	ok, err := exists(path)
	if err != nil {
		return err
	}
	if !ok {
		err = os.Mkdir(path, os.ModePerm)
	}
	return err
}

func CreateStorageJson(filePath string) error {
	if _, err := os.Stat(filePath); err != nil {
		if os.IsNotExist(err) {
			// file does not exist
			f, err := os.Create(filePath)
			if err != nil {
				return err
			}
			_, err = f.Write([]byte("[]"))
			if err != nil {
				return err
			}
			err = f.Close()
			if err != nil {
				return err
			}

		} else {
			// other error
			return err
		}
	}
	return nil
}
