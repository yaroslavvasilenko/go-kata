package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	LogLevel string
	Addr     string
}

// GetConf returns a new Config struct
func NewConfig() (*Config, error) {
	// Store the PATH environment variable in a variable
	if err := godotenv.Load("./module4/webserver/http/homework/.env"); err != nil {
		fmt.Println("No .env file found")
	}

	cfg := &Config{
		LogLevel: getVarEnv("LOG_LEVEL", "debug"),
		Addr:     getVarEnv("ADDR", ":8080"),
	}
	return cfg, nil
}

// Simple helper function to read an environment variable or return a default value
func getVarEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		fmt.Println(key + " = " + value)
		return value
	}
	fmt.Println("Default " + key + " = " + defaultVal)
	return defaultVal
}

func boolEnv(valEnv string) bool {
	if valEnv == "true" {
		return true
	} else {
		return false
	}
}
