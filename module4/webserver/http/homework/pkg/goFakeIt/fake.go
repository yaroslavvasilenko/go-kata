package goFakeIt

import (
	"github.com/brianvoe/gofakeit/v6"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/internal/enity/model"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/internal/usecase/repo"
	"math/rand"
)

func CreateFakeUsers(path string) error {
	var maxAge, minAge = 60, 18
	s := repo.StorageJSON{FilePath: path}
	users, err := s.GetUsers()
	if err != nil {
		return err
	}
	if len(users) < 10 {
		for i := 0; i < 10; i++ {
			u := &model.User{
				ID:       i,
				Name:     gofakeit.Name(),
				Age:      rand.Intn(maxAge-minAge) + minAge,
				Country:  gofakeit.Country(),
				Language: gofakeit.Language(),
			}
			_, err := s.CreateUser(u)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
