package main

import (
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/pkg/goFakeIt"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/until"
	"log"

	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/internal/app"

	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/pkg/config"
)

func init() {
	err := until.CreateStorageFile(app.StorageFilePath)
	if err != nil {
		log.Println(err)
	}

	err = until.CreateStorageJson(app.StorageJsonPath)
	if err != nil {
		log.Println(err)
	}

	err = goFakeIt.CreateFakeUsers(app.StorageJsonPath)
	if err != nil {
		log.Println(err)
	}
}

func main() {
	// Configuration
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatalf("Config error: %s", err)
	}

	// Run

	app.Run(cfg)
}
