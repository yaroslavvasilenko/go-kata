package model

type User struct {
	ID       int    `json:"id,omitempty"`
	Name     string `json:"name,omitempty"`
	Age      int    `json:"age,omitempty"`
	Country  string `json:"country,omitempty"`
	Language string `json:"language,omitempty"`
}
