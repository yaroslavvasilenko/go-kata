package service

import (
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/internal/enity/model"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/internal/usecase"
)

type JsonService struct {
	storageJson usecase.StorageJsonInterface
}

func (s *JsonService) GetUsers() ([]model.User, error) {
	return s.storageJson.GetUsers()

}

func (s *JsonService) GetUser(id int) (*model.User, error) {
	return s.storageJson.GetUser(id)
}

func (s *JsonService) CreateUser(user *model.User) (*model.User, error) {
	return s.CreateUser(user)
}
