package service

import (
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/internal/usecase"
	"mime/multipart"
)

type FileService struct {
	storageFile usecase.StorageFileInterface
}

func (s *FileService) SaveFile(f multipart.File, nameFile string) error {
	err := s.storageFile.SaveFile(f, nameFile)
	if err != nil {
		return err
	}
	return nil

}

func (s *FileService) GiveBackFile(path string) (string, error) {
	return s.storageFile.PathToFile(path)
}
