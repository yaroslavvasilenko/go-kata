package usecase

import (
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/internal/enity/model"
	"mime/multipart"
)

type StorageJsonInterface interface {
	GetUsers() ([]model.User, error)
	GetUser(id int) (*model.User, error)
	CreateUser(task *model.User) (*model.User, error)
}

type StorageFileInterface interface {
	SaveFile(f multipart.File, nameFile string) error
	PathToFile(name string) (string, error)
}

type ServiceJsonInterface interface {
	GetUsers() ([]model.User, error)
	GetUser(id int) (*model.User, error)
	CreateUser(user *model.User) (*model.User, error)
}

type ServiceFileInterface interface {
	SaveFile(f multipart.File, nameFile string) error
	PathToFile(name string) (string, error)
}
