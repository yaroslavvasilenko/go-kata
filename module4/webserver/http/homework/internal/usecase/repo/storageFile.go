package repo

import (
	"errors"
	"io"
	"mime/multipart"
	"os"
)

type StorageFile struct {
	FilePath string
}

func (sf *StorageFile) SaveFile(fileSave multipart.File, nameFile string) error {
	f, err := os.Create(sf.FilePath + nameFile)
	if err != nil {
		return err
	}
	_, err = io.Copy(f, fileSave)
	if err != nil {
		return err
	}

	return nil
}

func (sf *StorageFile) PathToFile(name string) (string, error) {
	path := sf.FilePath + name
	if !existFile(path) {
		return "", errors.New("file not exist")
	}
	return path, nil
}

func existFile(path string) bool {
	var exist bool = true
	_, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			exist = false
		}
	}
	return exist
}
