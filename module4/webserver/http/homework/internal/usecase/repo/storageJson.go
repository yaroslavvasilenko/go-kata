package repo

import (
	"encoding/json"
	"errors"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/internal/enity/model"
	"io"
	"os"
)

var NotFoundUser = errors.New("not found task")

// StorageJSON is a file-based implementation of TaskRepository
type StorageJSON struct {
	FilePath string
}

func NewFileTaskRepository(f string) *StorageJSON {
	return &StorageJSON{f}
}

// GetTasks returns all tasks from the repository
func (repo *StorageJSON) GetUsers() ([]model.User, error) {
	var users []model.User

	file, err := os.Open(repo.FilePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(content, &users); err != nil {
		return nil, err
	}

	return users, nil
}

// GetTask returns a single task by its ID
func (repo *StorageJSON) GetUser(id int) (*model.User, error) {
	users, err := repo.GetUsers()
	if err != nil {
		return nil, err
	}

	var user *model.User
	var found bool
	for _, t := range users {
		if t.ID == id {
			found = true
			user = &t
			break
		}
	}

	if !found {
		return nil, NotFoundUser
	}

	return user, nil
}

// CreateUser adds a new task to the repository
func (repo *StorageJSON) CreateUser(user *model.User) (*model.User, error) {
	users, err := repo.GetUsers()
	if err != nil {
		return user, err
	}

	user.ID = len(users) + 1
	users = append(users, *user)

	if err := repo.saveUsers(users); err != nil {
		return user, err
	}

	return user, nil
}

func (repo *StorageJSON) saveUsers(users []model.User) error {
	f, err := os.OpenFile(repo.FilePath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		return err
	}

	taskByte, err := json.Marshal(users)
	if err != nil {
		return err
	}

	_, err = f.Write(taskByte)
	if err != nil {
		return err
	}

	if err := f.Close(); err != nil {
		return err
	}
	return nil
}
