package app

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/internal/usecase"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/internal/usecase/repo"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/pkg/config"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/pkg/logger"
)

type Handlers struct {
	Storage     usecase.ServiceJsonInterface
	StorageFile usecase.ServiceFileInterface
}

const (
	StorageJsonPath = "module4/webserver/http/homework/serverStorage"
	StorageFilePath = "module4/webserver/http/homework/public/"
)

func Run(cfg *config.Config) {
	logger.New(cfg.LogLevel)
	logger.Log.Info("initialization server")

	storage := &repo.StorageJSON{FilePath: StorageJsonPath}
	storageFile := &repo.StorageFile{FilePath: StorageFilePath}
	a := Handlers{Storage: storage,
		StorageFile: storageFile}

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/", a.Hello)

	r.Route("/users", func(r chi.Router) {
		r.Get("/", a.GetUsers)
		r.Post("/", a.CreateUser)
		r.Get("/{id:[0-9]+}", a.GetUserForID)
	})

	r.Post("/upload", a.FileUpload)
	r.Get("/public/{fileName:[a-zA-z0-9]+}", a.FileDownload)

	port := ":8080"
	srv := &http.Server{
		Addr:    cfg.Addr,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		logger.Log.Info(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			logger.Log.Fatal(fmt.Errorf("listen: %s\n", err))
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	logger.Log.Info("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		logger.Log.Fatal("Server Shutdown:", err)
	}

	logger.Log.Info("Server exiting")
}
