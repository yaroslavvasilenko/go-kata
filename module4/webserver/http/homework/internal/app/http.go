package app

import (
	"encoding/json"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/pkg/logger"

	"github.com/go-chi/chi/v5"
	"gitlab.com/yaroslavvasilenko/go-kata/module4/webserver/http/homework/internal/enity/model"
	"io"
	"net/http"
	"strconv"
)

func (a *Handlers) Hello(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	_, err := w.Write([]byte(`{"welcome": "Hello mentor"}`))
	if err != nil {
		logger.Log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func (a *Handlers) GetUsers(w http.ResponseWriter, r *http.Request) {
	users, err := a.Storage.GetUsers()
	if err != nil {
		logger.Log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	usersJson, err := json.Marshal(users)
	if err != nil {
		logger.Log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Write(usersJson)
}

func (a *Handlers) CreateUser(w http.ResponseWriter, r *http.Request) {
	data, err := io.ReadAll(r.Body)
	if err != nil {
		logger.Log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	user := &model.User{}
	err = json.Unmarshal(data, user)
	if err != nil {
		logger.Log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	_, err = a.Storage.CreateUser(user)
	if err != nil {
		logger.Log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func (a *Handlers) GetUserForID(w http.ResponseWriter, r *http.Request) {
	idUserString := chi.URLParam(r, "id")
	idUser, err := strconv.Atoi(idUserString)
	if err != nil {
		logger.Log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return

	}
	user, err := a.Storage.GetUser(idUser)
	if err != nil {
		logger.Log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	userJson, err := json.Marshal(user)
	if err != nil {
		logger.Log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Write(userJson)
}

func (a *Handlers) FileUpload(w http.ResponseWriter, r *http.Request) {
	file, fileHeader, err := r.FormFile("file")
	if err != nil {
		logger.Log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer file.Close()

	err = a.StorageFile.SaveFile(file, fileHeader.Filename)
	if err != nil {
		logger.Log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

}

func (a *Handlers) FileDownload(w http.ResponseWriter, r *http.Request) {
	fileName := chi.URLParam(r, "fileName")

	pathToFile, err := a.StorageFile.PathToFile(fileName)
	if err != nil {
		logger.Log.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	http.ServeFile(w, r, pathToFile)

}
